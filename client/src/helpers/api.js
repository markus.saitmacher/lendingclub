import axios from 'axios';
import { sleepSeconds } from "../helpers/utilities"

const apiCreateLendingClubRoute = '/createLendingClub'
const apiGetUserAndHisLendingClubRoute = '/getUserAndHisLendingClub'
const apiGetEthAndDaiTestFundsRoute = '/getEthAndDaiTestFunds'
const apiUserPaidLendingClubRoute = '/apiUserPaidLendingClub'
const apiGetLendingClubArtifactsRoute = '/contracts/lendingclub.json'
const apiGetDaiTokenArtifactsRoute = '/contracts/daitoken.json'
const compoundDaiSupplyRateEndpoint = 'https://api.compound.finance/api/v2/ctoken?addresses[]=0x5d3a536e4d6dbd6114cc1ead35777bab948e3643'
const endPoint = process.env.REACT_APP_STAGE === 'dev' ? 'http://localhost:5000/' :  'http://api.lendingClubDapp.com/'
console.log('endPoint used', endPoint)
/**
 * Configuration for lendingClub api
 * @type axios instance
 */
const api = axios.create({
  baseURL: endPoint,
  timeout: 60000, // 60 secs
  headers: {
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache',
    'Accept': 'application/json',
  },
})

/**
 * @desc Gets Lending CLub artifacts
 * @return {Object}
 */
export const apiGetLendingClubArtifacts = async () => {
    const { data } = await api.get(apiGetLendingClubArtifactsRoute)
    return data
}

/**
 * @desc Gets Dai Token artifacts
 * @return {Object}
 */
export const apiGetDaiTokenArtifacts = async () => {
    const { data } = await api.get(apiGetDaiTokenArtifactsRoute)
    return data
}

/**
 * @desc Gets the current interest rate for dai from the compound api
 * @return {number}
 */
export const getDaiSupplyRate = async () => {
    try {
        const { data } = await axios.get(compoundDaiSupplyRateEndpoint)
        const { cToken } = data
        const { value } = cToken[0].supply_rate
        return parseFloat(value)
    } catch (e) {
        console.error('e', e)
        throw new Error(`Could not get cDai Rate ${e}`)
    }
}

/**
 * @desc calls the server to create the LendingClub
 * @desc then periodically checks until data was updated
 * @return {Object}
 */
export const apiCreateLendingClub = async (createLendingClubData) => {
  try {
    const createResponse = await api.post(apiCreateLendingClubRoute, { createLendingClubData } )
    if(createResponse.data.success) {
          const { mainAccountAddress } = createLendingClubData
          const userAndLendingClubDataResponse = await apiFetchUntilUpdate(apiGetUserAndHisLendingClub, mainAccountAddress )
          return userAndLendingClubDataResponse
    } else {
      throw new Error('Could not Create contract')
    }
  } catch (e) {
    console.error('apiCreateLendingClub', e)
    throw new Error(e)
  }
}


/**
 * @desc Check whether user already has created a Lending Club and return it
 * @desc will return null if no contract found
 * @return {Object}
 */
export const apiGetUserAndHisLendingClub = async (userAddress) => {
  return await api.post(apiGetUserAndHisLendingClubRoute, { userAddress })
}

/**
 * @desc Server sends 1 eth and 100 dai in testfunds to the user account
 * TODO return transaction ID to display to user
 * @return {Object}
 */
export const apiGetEthAndDaiTestFunds = async (userAddress) => {
  return await api.post(apiGetEthAndDaiTestFundsRoute, { userAddress, respondOnceTxsSettled: true })
}

export const apiUserPaidLendingClub = async (daiAmount, txHash, lendingClubAddress) => {
  return await api.post(apiUserPaidLendingClubRoute, { daiAmount, txHash, lendingClubAddress })
}

export const apiFetchUntilUpdate = async (fetchFunction, param) => {
  try {
    await sleepSeconds(1)
    let fetchedData = await fetchFunction(param)
    while (!fetchedData.data.success) {
      fetchedData = await fetchFunction(param)
      await sleepSeconds(1)
    }
    return fetchedData
  } catch (e) {
    console.error('apiCreateLendingClub', e)
    throw new Error(e)
  }
}


/**
 * @desc get metmask selected network
 * @return {Promise}
 */
export const apiGetMetamaskNetwork = async () => {
  try {
    if (typeof window.web3 !== 'undefined') {
      const networkID = await window.web3.eth.net.getNetworkType()
      return null
    } else {
      throw new Error('Window.web3 is undefined')
    }
  } catch (error) {
    return null //returning null here is fine as we fall back to an unsupported network
  }
}


// /**
//  * @desc get metmask selected network
//  * @return {Promise}
//  */
// export const apiGetMetamaskNetwork = async () =>
//   new Promise((resolve, reject) => {
//     if (typeof window.web3 !== 'undefined') {
//       window.web3.version.getNetwork((err, networkID) => {
//         if (err) {
//           console.error(err);
//           reject(err);
//         }
//         let networkIDList = {};
//         Object.keys(networks).forEach(network => {
//           networkIDList[networks[network].id] = network;
//         });
//         resolve(networkIDList[Number(networkID)] || null);
//       });
//     }
//   });

/**
 * Configuration for balance api
 * @type axios instance
 */
const old_api = axios.create({
  baseURL: 'https://indexer.balance.io',
  timeout: 30000, // 30 secs
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

/**
 * @desc get transaction data
 * @param  {String}   [address = '']
 * @param  {String}   [network = 'mainnet']
 * @param  {Number}   [page = 1]
 * @return {Promise}
 */
export const apiGetTransactionData = (
  address = '',
  network = 'mainnet',
  page = 1,
) => old_api.get(`/get_transactions/${network}/${address}/${page}`);

/**
 * @desc get transaction
 * @param  {String}   [txnHash = '']
 * @param  {String}   [network = 'mainnet']
 * @return {Promise}
 */
export const apiGetTransaction = (txnHash = '', network = 'mainnet') =>
  old_api.get(`/get_transaction/${network}/${txnHash}`);

/**
 * @desc get ethereum gas prices
 * @return {Promise}
 */
export const apiGetGasPrices = () => old_api.get(`/get_eth_gas_prices`);
