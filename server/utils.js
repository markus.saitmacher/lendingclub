module.exports = class Utils {

    static async getDaiBalance(dai, account) {
        const daiBalance = await dai.balanceOf.call(account)
        return daiBalance
    }

    static async withdrawContractDaiBalanceToOwner(lc, account) {
        const withdrawTx = await lc.withdrawContractDaiBalanceToOwner({ from: account })
        return withdrawTx
    }

    static async sleepSeconds(seconds) {
        const actualSeconds = seconds * 1000
        return new Promise(resolve => setTimeout(resolve, actualSeconds));
    }

    static async depositDaiToLendingClub(lc, amount, account) {
        const depositedDaiTx = await lc.depositDaiToLendingClub(amount, { from: account })
        return depositedDaiTx
    }

    static async getPublicVariableFromContract(contract, variable) {
        const contractVariable = await contract[variable].call()
        return contractVariable
    }

    static async initializeLendingClub(lc, participants, amountPerPersonInDai, timeIntervalSeconds, account) {
        const setAccountTx = await lc.initializeLendingClub(participants, amountPerPersonInDai, timeIntervalSeconds, { from: account })
        return setAccountTx
    }

    static async resetLendingClubContract(lc, account) {
        const resetTx = await lc.reset({ from: account })
        return resetTx
    }

    static async approveAddressForDaiSpending(dai, lendingClubAddress, amount, account) {
        const approvalTx = await dai.approve(lendingClubAddress, amount, { from: account })
        return approvalTx
    }

    static async getDaiAllowanceForAddress(dai, account, lendingClubAddress) {
        const allowance = await dai.allowance(account, lendingClubAddress)
        return allowance
    }

    static async getAllDaiBalances(dai, accounts) {
        const promises = []
        accounts.forEach(account => {
            promises.push(dai.balanceOf.call(account))
        })
        const balances = await Promise.all(promises)
        return balances
    }

    static async transferDaiToAddress(dai, to, amount, account) {
        const transferTx = await dai.transfer(to, amount, { from: account })
        return transferTx
    }

    static isObjectEmpty(obj) {
        return obj == null || (Object.entries(obj).length === 0 && obj.constructor === Object)
    }

    static async depositWithExpectedFailure(lc, amount, account) {
        try {
            const shouldNotResolve = await this.depositDaiToLendingClub(lc, amount, account)
            return shouldNotResolve
        } catch (e) {
            return e
        }
    }
}