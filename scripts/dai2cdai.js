// init web3, require ABI JSON, and declare wallet constants
const Web3 = require('web3');
const Tx = require('ethereumjs-tx').Transaction
// const Maker = require('@makerdao/dai')

const cDaiABI = '[ { constant: true,inputs: [],name: "name",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x06fdde03" },{ constant: false,inputs: [ [Object], [Object] ],name: "approve",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0x095ea7b3" },{ constant: false,inputs: [ [Object] ],name: "repayBorrow",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0x0e752702" },{ constant: true,inputs: [],name: "reserveFactorMantissa",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x173b9904" },{ constant: false,inputs: [ [Object] ],name: "borrowBalanceCurrent",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0x17bfdfbc" },{ constant: true,inputs: [],name: "totalSupply",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x18160ddd" },{ constant: true,inputs: [],name: "exchangeRateStored",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x182df0f5" },{ constant: false,inputs: [ [Object], [Object], [Object] ],name: "transferFrom",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0x23b872dd" },{ constant: false,inputs: [ [Object], [Object] ],name: "repayBorrowBehalf",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0x2608f818" },{ constant: true,inputs: [],name: "pendingAdmin",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x26782247" },{ constant: true,inputs: [],name: "decimals",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x313ce567" },{ constant: false,inputs: [ [Object] ],name: "balanceOfUnderlying",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0x3af9e669" },{ constant: true,inputs: [],name: "getCash",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x3b1d21a2" },{ constant: false,inputs: [ [Object] ],name: "_setComptroller",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0x4576b5db" },{ constant: true,inputs: [],name: "totalBorrows",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x47bd3718" },{ constant: true,inputs: [],name: "comptroller",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x5fe3b567" },{ constant: false,inputs: [ [Object] ],name: "_reduceReserves",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0x601a0bf1" },{ constant: true,inputs: [],name: "initialExchangeRateMantissa",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x675d972c" },{ constant: true,inputs: [],name: "accrualBlockNumber",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x6c540baf" },{ constant: true,inputs: [],name: "underlying",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x6f307dc3" },{ constant: true,inputs: [ [Object] ],name: "balanceOf",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x70a08231" },{ constant: false,inputs: [],name: "totalBorrowsCurrent",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0x73acee98" },{ constant: false,inputs: [ [Object] ],name: "redeemUnderlying",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0x852a12e3" },{ constant: true,inputs: [],name: "totalReserves",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x8f840ddd" },{ constant: true,inputs: [],name: "symbol",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x95d89b41" },{ constant: true,inputs: [ [Object] ],name: "borrowBalanceStored",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0x95dd9193" },{ constant: false,inputs: [ [Object] ],name: "setTotalBorrows",outputs: [],payable: false,stateMutability: "nonpayable",type: "function",signature: "0x977e19e9" },{ constant: false,inputs: [ [Object] ],name: "mint",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0xa0712d68" },{ constant: false,inputs: [],name: "accrueInterest",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0xa6afed95" },{ constant: false,inputs: [ [Object], [Object] ],name: "transfer",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0xa9059cbb" },{ constant: true,inputs: [],name: "borrowIndex",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0xaa5af0fd" },{ constant: true,inputs: [],name: "supplyRatePerBlock",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0xae9d70b0" },{ constant: false,inputs: [ [Object] ],name: "setTotalReserves",outputs: [],payable: false,stateMutability: "nonpayable",type: "function",signature: "0xb0e0bf2a" },{ constant: false,inputs: [ [Object], [Object], [Object] ],name: "seize",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0xb2a02ff1" },{ constant: false,inputs: [ [Object] ],name: "_setPendingAdmin",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0xb71d1a0c" },{ constant: false,inputs: [],name: "exchangeRateCurrent",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0xbd6d894d" },{ constant: true,inputs: [ [Object] ],name: "getAccountSnapshot",outputs: [ [Object], [Object], [Object], [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0xc37f68e2" },{ constant: false,inputs: [ [Object] ],name: "borrow",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0xc5ebeaec" },{ constant: false,inputs: [ [Object] ],name: "redeem",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0xdb006a75" },{ constant: true,inputs: [ [Object], [Object] ],name: "allowance",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0xdd62ed3e" },{ constant: false,inputs: [],name: "_acceptAdmin",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0xe9c714f2" },{ constant: false,inputs: [ [Object] ],name: "_setInterestRateModel",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0xf2b3abbd" },{ constant: true,inputs: [],name: "interestRateModel",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0xf3fdb15a" },{ constant: false,inputs: [ [Object], [Object], [Object] ],name: "liquidateBorrow",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0xf5e3c462" },{ constant: true,inputs: [],name: "admin",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0xf851a440" },{ constant: true,inputs: [],name: "borrowRatePerBlock",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0xf8f9da28" },{ constant: false,inputs: [ [Object] ],name: "_setReserveFactor",outputs: [ [Object] ],payable: false,stateMutability: "nonpayable",type: "function",signature: "0xfca7820b" },{ constant: true,inputs: [],name: "isCToken",outputs: [ [Object] ],payable: false,stateMutability: "view",type: "function",signature: "0xfe9c44ae" },{ inputs:[ [Object],[Object],[Object],[Object],[Object],[Object],[Object],[Object] ],payable: false,stateMutability: "nonpayable",type: "constructor",constant: undefined },{ anonymous: false,inputs: [ [Object], [Object], [Object] ],name: "AccrueInterest",type: "event",constant: undefined,payable: undefined,signature:"0x875352fb3fadeb8c0be7cbbe8ff761b308fa7033470cd0287f02f3436fd76cb9" },{ anonymous: false,inputs: [ [Object], [Object], [Object] ],name: "Mint",type: "event",constant: undefined,payable: undefined,signature:"0x4c209b5fc8ad50758f13e2e1088ba56a560dff690a1c6fef26394f4c03821c4f" },{ anonymous: false,inputs: [ [Object], [Object], [Object] ],name: "Redeem",type: "event",constant: undefined,payable: undefined,signature:"0xe5b754fb1abb7f01b499791d0b820ae3b6af3424ac1c59768edb53f4ec31a929" },{ anonymous: false,inputs: [ [Object], [Object], [Object], [Object] ],name: "Borrow",type: "event",constant: undefined,payable: undefined,signature:"0x13ed6866d4e1ee6da46f845c46d7e54120883d75c5ea9a2dacc1c4ca8984ab80" },{ anonymous: false,inputs: [ [Object], [Object], [Object], [Object], [Object] ],name: "RepayBorrow",type: "event",constant: undefined,payable: undefined,signature:"0x1a2a22cb034d26d1854bdc6666a5b91fe25efbbb5dcad3b0355478d6f5c362a1" },{ anonymous: false,inputs: [ [Object], [Object], [Object], [Object], [Object] ],name: "LiquidateBorrow",type: "event",constant: undefined,payable: undefined,signature:"0x298637f684da70674f26509b10f07ec2fbc77a335ab1e7d6215a4b2484d8bb52" },{ anonymous: false,inputs: [ [Object], [Object] ],name: "NewPendingAdmin",type: "event",constant: undefined,payable: undefined,signature:"0xca4f2f25d0898edd99413412fb94012f9e54ec8142f9b093e7720646a95b16a9" },{ anonymous: false,inputs: [ [Object], [Object] ],name: "NewAdmin",type: "event",constant: undefined,payable: undefined,signature:"0xf9ffabca9c8276e99321725bcb43fb076a6c66a54b7f21c4e8146d8519b417dc" },{ anonymous: false,inputs: [ [Object], [Object] ],name: "NewComptroller",type: "event",constant: undefined,payable: undefined,signature:"0x7ac369dbd14fa5ea3f473ed67cc9d598964a77501540ba6751eb0b3decf5870d" },{ anonymous: false,inputs: [ [Object], [Object] ],name: "NewMarketInterestRateModel",type: "event",constant: undefined,payable: undefined,signature:"0xedffc32e068c7c95dfd4bdfd5c4d939a084d6b11c4199eac8436ed234d72f926" },{ anonymous: false,inputs: [ [Object], [Object] ],name: "NewReserveFactor",type: "event",constant: undefined,payable: undefined,signature:"0xaaa68312e2ea9d50e16af5068410ab56e1a1fd06037b1a35664812c30f821460" },{ anonymous: false,inputs: [ [Object], [Object], [Object] ],name: "ReservesReduced",type: "event",constant: undefined,payable: undefined,signature:"0x3bad0c59cf2f06e7314077049f48a93578cd16f5ef92329f1dab1420a99c177e" },{ anonymous: false,inputs: [ [Object], [Object], [Object] ],name: "Failure",type: "event",constant: undefined,payable: undefined,signature:"0x45b96fe442630264581b197e84bbada861235052c5a1aadfff9ea4e40a969aa0" },{ anonymous: false,inputs: [ [Object], [Object], [Object] ],name: "Transfer",type: "event",constant: undefined,payable: undefined,signature:"0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef" },{ anonymous: false,inputs: [ [Object], [Object], [Object] ],name: "Approval",type: "event",constant: undefined,payable: undefined,signature:"0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925" } ] '

const erc20ABI = '[ { "constant": true, "inputs": [], "name": "name", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "_spender", "type": "address" }, { "name": "_value", "type": "uint256" } ], "name": "approve", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "totalSupply", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "_from", "type": "address" }, { "name": "_to", "type": "address" }, { "name": "_value", "type": "uint256" } ], "name": "transferFrom", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "decimals", "outputs": [ { "name": "", "type": "uint8" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_owner", "type": "address" } ], "name": "balanceOf", "outputs": [ { "name": "balance", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "symbol", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "_to", "type": "address" }, { "name": "_value", "type": "uint256" } ], "name": "transfer", "outputs": [ { "name": "", "type": "bool" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [ { "name": "_owner", "type": "address" }, { "name": "_spender", "type": "address" } ], "name": "allowance", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "payable": true, "stateMutability": "payable", "type": "fallback" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "owner", "type": "address" }, { "indexed": true, "name": "spender", "type": "address" }, { "indexed": false, "name": "value", "type": "uint256" } ], "name": "Approval", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "from", "type": "address" }, { "indexed": true, "name": "to", "type": "address" }, { "indexed": false, "name": "value", "type": "uint256" } ], "name": "Transfer", "type": "event" } ]'


// Constants
//make sure not ropsten
const web3 = new Web3(new Web3.providers.HttpProvider(httpProvider))

// const httpProvider = 'https://kovan.infura.io/v3/ffa771bbbe4344ed88d8dd38d8cf3f55'
// const accountAddress = '0x32305396644FC469028Fb83adb56f7cc53153336'
// const privateKey = '593F40264EEF19F298293F704B2B1746AA92585481C56F34CC3912E5F4676F37'
// const daiAddress = '0xbF7A7169562078c96f0eC1A8aFD6aE50f12e5A99'
// const cDaiAddress = '0x0A1e4D0B5c71B955c0a5993023fc48bA6E380496'

const httpProvider = 'http://localhost:8545'
const accountAddress = '0x2BC5DAc2080c6b0B5Bc025E0B894d60FDb04A260'
const privateKey = '0x3d67ec6401a258dffaee67e89f7b00c32c705d64af5e7d49bcc8248e62130bec'
const daiAddress = '0x80f76cB60B4e1dAcdb88bc26fC340ce2561D6e4A'
const cDaiAddress = '0xc653A5365D7079876DA2003C4D7f1939291353e4'

const bufferPrivateKey = Buffer.from(privateKey, 'hex');
const toAddress = '0x4a8Ee04161BCA324D60464E8A79E2Ca1d6316dBb'

// const cDaiInstance = new web3.eth.Contract(JSON.parse(cDaiABI), cDaiAddress)
const daiInstance = new web3.eth.Contract(JSON.parse(erc20ABI), daiAddress)

// const tx = contract.methods.createExchange(tokenAddress)

const cDaiMintAmount = 1000000000000
const daiAllowanceAmount = 10000000000000

//First, we have get approval from the dai contract to let the cToken contract use it
async function daiManualAllowance() {
    try {
        const allowanceDaiTx = daiInstance.methods.approve(cDaiAddress, daiAllowanceAmount)
        const allowanceDaiEncodedABI = allowanceDaiTx.encodeABI()
        const nonce = await web3.eth.getTransactionCount(accountAddress)
        const allowanceTransactionData = {
            nonce: nonce,
            gasLimit: web3.utils.toHex(1000000),
            gasPrice: web3.utils.toHex(12e9),
            to: daiAddress,
            from: accountAddress,
            chainId: 42,
            data: allowanceDaiEncodedABI,
        }
        const transaction = new Tx(allowanceTransactionData)
        transaction.sign(bufferPrivateKey)
        const serializedTx = transaction.serialize().toString('hex')
        console.log('got here')
        const txResult = await web3.eth.sendSignedTransaction('0x' + serializedTx)
        console.log('txResult', txResult)
    } catch (e) {
        console.error('error', e)
    }
}




async function mintCDai() {
    try {
        const mintCDaiTx = cDaiInstance.methods.mint(cDaiMintAmount)
        const mintCDaiEncodedABI = mintCDaiTx.encodeABI()
        const nonce = await web3.eth.getTransactionCount(accountAddress)
        const mintTransactionData = {
            nonce: nonce,
            gasLimit: web3.utils.toHex(1000000),
            gasPrice: web3.utils.toHex(12e9),
            to: cDaiAddress,
            from: accountAddress,
            // value: amount,
            chainId: 42,
            data: mintCDaiEncodedABI,
        }
        const transaction = new Tx(mintTransactionData)
        transaction.sign(bufferPrivateKey)
        const serializedTx = transaction.serialize().toString('hex')
        const txResult = await web3.eth.sendSignedTransaction('0x' + serializedTx)
        console.log('txResult', txResult)

    } catch (e) {
        console.error('error', e)
    }

}
//must allow smart contract as well!
// https://ethereum.stackexchange.com/questions/46318/how-can-i-transfer-erc20-tokens-from-a-contract-to-an-user-account
async function transferCDaiToAddress() {
    try {
        const source = accountAddress
        const destination = toAddress
        const transferFromTx = cDaiInstance.methods.transferFrom(source, destination, cDaiMintAmount)
        const transferFromEncodedABI = transferFromTx.encodeABI()
        const nonce = await web3.eth.getTransactionCount(accountAddress)
        const mintTransactionData = {
            nonce: nonce,
            gasLimit: web3.utils.toHex(1000000),
            gasPrice: web3.utils.toHex(12e9),
            to: cDaiAddress,
            from: accountAddress,
            chainId: 42,
            data: transferFromEncodedABI,
        }
        const transaction = new Tx(mintTransactionData)
        transaction.sign(bufferPrivateKey)
        const serializedTx = transaction.serialize().toString('hex')
        const txResult = await web3.eth.sendSignedTransaction('0x' + serializedTx)
        console.log('txResult', txResult)

    } catch (e) {
        console.error('error', e)
    }
}



//current cDaiBalanceTx 4838
async function getCDaiBalance(address) {
    try {
        const cDaiBalanceTx = await cDaiInstance.methods.balanceOf(address).call()
        console.log('cDaiBalanceTx', cDaiBalanceTx)
    } catch (e) {
        console.error('error', e)
    }

}

async function getTotalCDaiSupply() {
    try {
        const cDaiTotalSupply = await cDaiInstance.methods.totalSupply().call()
        console.log('cDaiTotalSupply',cDaiTotalSupply )
    } catch (e) {
        console.error('error', e)
    }

}

async function getCDaiAddress(){
    // cat development.json | grep PriceOracle
    const priceOracleAddress = "0xb29D625e93A385C539A2Bb6f93b2CcbaA8eD0Bf2"
    const priceOracleInstant = new web3.eth.Contract(JSON.parse(priceOracleProxyAbi), priceOracleAddress)
    console.log('methods', priceOracleInstant.methods)
    const cDaiTotalSupply = await priceOracleInstant.methods.cDaiAddress().call()
    console.log('cDaiTotalSupply',cDaiTotalSupply)

}



daiManualAllowance()
// mintCDai()


// getCDaiBalance(accountAddress)
// getCDaiBalance(toAddress)
// getTotalCDaiSupply()

// transferCDaiToAddress()
// getCDaiAddress()
