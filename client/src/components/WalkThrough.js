import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled, { keyframes } from 'styled-components';
import Joyride, { ACTIONS, EVENTS, STATUS } from 'react-joyride';
import { updateWalkthroughStepIndex, stopWalkthrough } from '../reducers/_walkthrough';


// import { colors, fonts, responsive } from '../styles';
const walkThroughSteps = [
  {
    target: ".first-create-step",
    content: "First, you would type in your name and the name of all the participants of your Lending Club",
    disableBeacon: false,
   // placement: 'right',
  },
  {
    target: ".second-create-step",
    content: "Now you can edit the Ethereum addresses of all participants in your Lending Club. \
             Random addresses are generated for testing purposes",
    disableBeacon: false,
  },
  {
    target: ".third-create-step",
    content: "You can define a time frame for your lending period, which we will skip for now",
    disableBeacon: false,
  },
  {
    target: ".fourth-create-step",
    content: "Here You can enter the amount everyone has to pay on each round",
    disableBeacon: false,
  },
  {
    target: ".fifth-create-step",
    content: "Now create your lending club!",
    disableBeacon: false,
  },
  {
    target: ".first-data-step",
    content: "This is the Lending Club you just created, as you can see no one has paid yet,\
               click next to see how to fund it",
    disableBeacon: false,
    placement: 'right',
  },
  {
    target: ".second-data-step",
    content: "By Clicking on PayLending Club you can send funds (dai) directly to the Lendingclub \
                in reality your friends would send you money beforehand",
    placement: 'right',
    disableBeacon: false,
  },
  {
    target: ".third-data-step",
    content: (<>
                <p>Finally, on this page all transactions will be recorded Now install </p>
                  <a href="https://metamask.io/" target="_blank">Meta Mask </a>
                <p> and connect to the rpc provide at blockchain.lendingclubdapp.com </p>
              </>),
    placement: 'right',
    disableBeacon: false,
  },
]
const stepsForCreatePage = 5

class WalkThrough extends Component {

    handleCreateJoyrideCallback = data => {
        const { action, index, status, type } = data;
        const nextIndex = index + (action === ACTIONS.PREV ? -1 : 1)

        if([ACTIONS.NEXT].includes(action) && [EVENTS.STEP_AFTER].includes(type)) {
            const reachedLastStepOfRoute = index >= stepsForCreatePage - 1 //offset
            if(reachedLastStepOfRoute){
                this.props.callCreateLendingClub()
            } else if(this.props.createStep < stepsForCreatePage){
                this.props.nextStep('tour')
            }
            this.props.updateWalkthroughStepIndex(nextIndex)
        } else if (action === 'prev' && [EVENTS.STEP_AFTER].includes(type)) {
            this.props.prevStep('tour')
            this.props.updateWalkthroughStepIndex(nextIndex)
        }

        if ([EVENTS.TARGET_NOT_FOUND].includes(type)) {
        }
    }

    handleDataJoyrideCallback = data => {
        const { action, index, status, type } = data;
        const nextIndex = index + (action === ACTIONS.PREV ? -1 : 1)
        const stepsForDataPage = 5
        if([ACTIONS.NEXT].includes(action) && [EVENTS.STEP_AFTER].includes(type)) {
          switch(index) {
            case 5:
                this.props.onViewFundClick()
                break
            case 6:
                this.props.onViewTxTabClick()
                break
          }
          this.props.updateWalkthroughStepIndex(nextIndex)
        }


    }


    handleCallBackDecider = (data) => {
      const { index } = data
      if(index < stepsForCreatePage) {
        this.handleCreateJoyrideCallback(data)
      }else {
        this.handleDataJoyrideCallback(data)
      }
    }

  render() {
        return (
            <Joyride
                stepIndex={this.props.walkThroughStepIndex}
                callback={this.handleCallBackDecider}
                steps={walkThroughSteps}
                run={this.props.walkthroughRun}
                continuous
                scrollToFirstStep
                showProgress
                showSkipButton
                styles={{
                    options: {
                        zIndex: 10000,
                        arrowColor: '#1990ff',
                        primaryColor:'#1990ff',
                        beaconSize: 50,
                    }
                }}
            />
    )
    }
}


const reduxProps = ({ walkthrough }) => ({
    walkthroughRun: walkthrough.run,
    walkThroughStepIndex: walkthrough.stepIndex,
    stopWalkthrough: walkthrough.stopWalkthrough,

});

export default connect(
    reduxProps,
    { updateWalkthroughStepIndex, stopWalkthrough },
)(WalkThrough);
