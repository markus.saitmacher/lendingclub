const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const utils = require('./utils.js')

const adapter = new FileSync('db.json')
const db = low(adapter)

const c = require('./constants')

const schema =
{
  "users": [],
  "lendingClubs": []
}


// Queries the user by address and if not present adds to db
// could set overwrite flag here on else
const addOrSetUser = async (user) => {
    try {
        //TODO assert that user has all necessary keys or instance Of User
        const userAddress = user[c.USER_ADDRESS]
        const queriedUser = await db.get(c.USERS)
            .find({ userAddress })
            .value()
        if (utils.isObjectEmpty(queriedUser)) {
            await db.get(c.USERS)
                .push(user)
                .write()
        } else {
            await db.get(c.USERS)
                .find({ userAddress })
                .assign(user)
                .write()
        }
        const msg = { success: true, user }
        return msg
    } catch (error) {
        console.error('error', error)
        const msg = { success: false, error: `Could not add or set User with address: ${user[c.USER_ADDRESS]}, error: ${error}` }
        return msg
    }
}


const performLendingClubCreated = async (lendingClubData) => {
    try {
        const { contractParticipants, lendingClubAddress, mainAccountAddress } = lendingClubData

        const addLendingClubMsg = await addOrSetLendingClub(lendingClubData)
        if (!addLendingClubMsg.success) throw new Error(addLendingClubMsg.message)

        const addAllUsersMsg = await addOrSetLendingClubUsersToDB(contractParticipants, mainAccountAddress, lendingClubAddress)
        if (!addAllUsersMsg.success) throw new Error(addAllUsersMsg.message)
        const msg = {success: true }
        return msg
    } catch (error) {
        console.error('error', error)
        const msg = { success: false, error: `Could not performLendingClubCreated for address: ${lendingClubAddress}, error: ${error}` }
        return msg
    }

}




// Queries the lendingClub by address and if not present adds to db
// could set overwrite flag here on else
const addOrSetLendingClub = async (lendingClub) => {
    try {
        //TODO assert that lendingClub has all necessary keys or instance Of lendingClub
        const lendingClubAddress = lendingClub[c.LENDING_CLUB_ADDRESS]
        const queriedLendingClub = await db.get(c.LENDING_CLUBS)
            .find({ lendingClubAddress })
            .value()
        if (utils.isObjectEmpty(queriedLendingClub)) {
            await db.get(c.LENDING_CLUBS)
                .push(lendingClub)
                .write()
        } else {
            await db.get(c.LENDING_CLUBS)
                .find({ lendingClubAddress })
                .assign(lendingClub)
                .write()
        }
        const msg = { success: true, lendingClub }
        return msg
    } catch (error) {
        console.error('error', error)
        const msg = { success: false, error: `Could not add or set lendingClub with address: ${lendingClub[c.LENDING_CLUB_ADDRESS]}, error:  ${error}` }
        return msg
    }
}

const updateLendingClubOnUserPayment = async (lendingClubAddress, txData) => {
    try {
        const queriedLendingClub = await db.get(c.LENDING_CLUBS)
            .find({ lendingClubAddress })
            .value()
        if (utils.isObjectEmpty(queriedLendingClub)) {
            throw new Error(`Could not find lending Club with address:${lendingClubAddress}`)
        }
        const { winners, currentRound, startEpochEarnSeconds, contractParticipants, contractParticipantsArray } = queriedLendingClub
        const names = Object.keys(contractParticipants)

        if (!startEpochEarnSeconds) queriedLendingClub.startEpochEarnSeconds = Math.round(new Date().getTime() / 1000)
        if (!queriedLendingClub.txData) queriedLendingClub.txData = []
        queriedLendingClub.txData.push(txData)
        //TODO add actual winners from passed on events or fetch events from ap
        if (currentRound === 0) {
            queriedLendingClub.winners = []
        } else if(currentRound > 0 && currentRound < contractParticipantsArray.length - 1  ) {
            const currentWinnersIndex = winners.length
            queriedLendingClub.winners.push(names[currentWinnersIndex])
        }  else {
            //Ended everyone won
            queriedLendingClub.winners = names
        }
        queriedLendingClub.currentRound = currentRound + 1
        const updatedLendingClub = await db.get(c.LENDING_CLUBS)
            .find({ lendingClubAddress })
            .assign(queriedLendingClub)
            .write()
        return { success: true, updatedLendingClub }
    } catch (error) {
        console.error('error', error)
        const msg = { success: false, error }
        return msg
    }
}




const addOrSetLendingClubUsersToDB = async (contractParticipants, mainAccountAddress, lendingClubAddress) => {
    try {
        const contractParticipantsEntries = Object.entries(contractParticipants)
        for (const [name, userAddress] of contractParticipantsEntries) {
            const isMainOfCurrentLendingClub = userAddress === mainAccountAddress
            const user = {
                userAddress,
                name,
                lendingClubAddress,
                isMainOfCurrentLendingClub,
            }
            const addUserMsg = await addOrSetUser(user)
            if (!addUserMsg.success) throw new Error(`Could not save user with address ${userAddress} to db`)
        }
        const msg = { success: true, contractParticipantsEntries }
        return msg
    } catch (error) {
        console.error('error', error)
        const msg = { success: false, error }
        return msg
    }
}


const getUserAndHisLendingClub = async (userAddress) => {
    try {
        const queriedUser = await db.get(c.USERS)
            .find({ userAddress })
            .value()
        if (utils.isObjectEmpty(queriedUser)) {
            return { success: false, message: `User does not exist in db, address: ${userAddress}` }
        } else {
            const { lendingClubAddress } = queriedUser
            const queriedLendingClub = await db.get(c.LENDING_CLUBS)
                .find({ lendingClubAddress })
                .value()
            if (!lendingClubAddress || utils.isObjectEmpty(queriedLendingClub)) {
                return { success: false, message: `User does not have a lending club yet, address: ${userAddress} ` }
            } else {
                const msg = { success: true, userAndLendingClubData: { user: queriedUser, lendingClub: queriedLendingClub } }
                return msg
            }
        }
    } catch (error) {
        console.error('error', error)
        const msg = { success: false, error }
        return msg
    }
}









module.exports = {
    getUserAndHisLendingClub,
    addOrSetLendingClubUsersToDB,
    addOrSetUser,
    addOrSetLendingClub,
    updateLendingClubOnUserPayment,
    performLendingClubCreated,
}

// const test = async () => {
//     const newUser = { userAddress: "0x0zzzzz", name: 'Daniel', lendingClubAddress: '0xxyyy', isMainOfCurrentLendingClub: false }
//     const newLendingClub = { lendingClubAddress: "0x0zzz", contractParticipants:  {'Tom': "0xx" }, amountPerPersonInDai: 9, timeIntervalSeconds: 0 }
//     let resp = await addUniqueUser(newUser)
//     resp = await addUniqueLendingClub(newLendingClub)
//     console.log('resp2', resp2)
//     userAddress = "0x0xxxx"
//     resp = await getUserAndHisLendingClub(userAddress)
//     const contractParticipants = {
//         "max": "0x00",
//         "tom": "0x0zzz",
//         "daniel": "0x0ooo",
//     }
//     const mainAccountAddress =  "0x00"
//     const lendingClubAddress = "0x0ccc"
//     resp = await addLendingClubUsersToDB(contractParticipants, mainAccountAddress, lendingClubAddress)
//     resp = await setUser(newUser)
//     resp = await setLendingClub(newLendingClub)
//     resp = await addOrSetUser(newUser)
//     resp = await addOrSetLendingClub(newLendingClub)
//     lendingClubAddress = "0xa6e1515343EA3985F702Eaea91976cB2b53DFA48"
//     resp = await updateLendingClubOnUserPayment(lendingClubAddress, 'currentRound', 0)
//     console.log('resp', resp)
// }
// test()