import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Home from './pages/Home';
import LendingClubCreate from './pages/LendingClubCreate';
import LendingClubData from './pages/LendingClubData'
import NotFound from './pages/NotFound';
import { warningOnline, warningOffline } from './reducers/_warning';

class Router extends Component {
  componentDidMount() {
    // window.browserHistory = this.context.router.history;
    window.onoffline = () => this.props.warningOffline();
    window.ononline = () => this.props.warningOnline();
  }

  render = () => {
    const { walkthroughRun, address, lendingClubContractData }  = this.props
    return (
      <Switch>
        <Route
          exact
          path="/create"
          render={routerProps => {
            if(walkthroughRun && !lendingClubContractData) {
              return <LendingClubCreate {...routerProps} />;
            } else if (lendingClubContractData) {
              return <Redirect to="/data" />
            } else if(!address) {
              return <Redirect to="/" />;
            }
            return <LendingClubCreate {...routerProps} />;
          }}
        />
        <Route
          exact
          path="/"
          render={routerProps => {
            if(lendingClubContractData) {
              return <Redirect to="/data" />
            } else if(address) {
              return <Redirect to="/create" />;
            }
            return <Home {...routerProps} />;
          }}
        />

        <Route
          exact
          path="/data"
          render={routerProps => {
            if (!lendingClubContractData) {
              return <Redirect to="/" />;
            }
            return <LendingClubData {...routerProps} />;
          }}
        />
        <Route component={NotFound} />
      </Switch>
    );
  };
}

Router.contextTypes = {
  router: PropTypes.object.isRequired
};

const reduxProps = ({ account, walkthrough }) => ({
  address: account.address,
  lendingClubContractData : account.lendingClubContractData,
  walkthroughRun: walkthrough.run
});

export default withRouter(
  connect(
    reduxProps,
    {
      warningOffline,
      warningOnline
    }
  )(Router)
);
