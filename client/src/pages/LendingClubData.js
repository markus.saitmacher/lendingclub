import React, { Component } from 'react';
import {
  requestCreateNewLendingClub,
  getDaiAndEthTestFunds,
  payLendingClub,
} from '../reducers/_account';
import { connect } from 'react-redux';
import BaseLayout from '../layouts/base';
import styled from 'styled-components';
import { Row, Table, Button, Card, Icon, Avatar, Progress } from 'antd';
import WalkThrough from '../components/WalkThrough'
import { getDaiSupplyRate } from '../helpers/api'
import { set } from 'shelljs';

const { Meta } = Card;
const StyledWrapper = styled.div`
  margin-top: 10%;
  display: flex;
  flex-direction: column;
  padding: 20px;
  text-align: center;
`;

const RowFixedHeight = styled(Row)`
    height: 75px !important;
`
Row.FixedHeight = RowFixedHeight

const StyledCard = styled(Card)`
  width: 405px;
  border: 0px !important;
  border-radius: 10px !important;
  box-shadow: 0px 12px 30px 3px;
  rgba(169, 168, 168, 0.41);
`
const IconStyled = styled(Icon)`
  &:hover {
    transform: translateY(-5px);
    color: #8567C3;
    a { color: #8567C3; }

    &::after {
      transform: scale(1);
    }
  }
  text-align: center;
  cursor: pointer;
`
Card.Styled = StyledCard
Icon.Styled = IconStyled

const columns = [
  {
    title: 'TxHash',
    dataIndex: 'hash',
    render: ethTxHash => <a target="_blank" href={`https://etherscan.io/tx/${ethTxHash}`}>{ethTxHash.slice(0, 5)}</a>,
  },
  //TODO put user name here
  {
    title: 'From',
    dataIndex: 'from',
    render: ethAddress => <a target="_blank" href={`https://etherscan.io/address/${ethAddress}`}>{ethAddress.slice(0, 5)}</a>,
  },
  {
    title: 'daiAmount',
    dataIndex: 'daiAmount',
  },
  {
    title: 'Gas used',
    dataIndex: 'gas',
  },

];
class LendingClubData extends Component {
  state = {
    currentTab: 'data',
    currentAmount: 0,
    timer: null,
    // ref: useRef(null),
  }

  sleepMs = async (seconds) => {
    const actualSeconds = seconds
    return new Promise(resolve => setTimeout(resolve, actualSeconds));
  }

  nowSeconds = () => Math.round(new Date().getTime() / 1000)

  componentDidMount = async () => {
      const { lendingClub } = this.props.lendingClubContractData
      await this.initCurrentAmount(lendingClub)
  }

  initCurrentAmount = async (lendingClub) => {
    //We funded and are earning interest now
    const { contractParticipantsArray, amountPerPersonInDai, startEpochEarnSeconds, currentRound } = lendingClub
    if (currentRound > 0 && currentRound <= contractParticipantsArray.length - 1) {
      const interestRate = await getDaiSupplyRate()
      const startAmount = Math.round(contractParticipantsArray.length * amountPerPersonInDai)
      this.calculateCurrentAmount(startAmount, interestRate, startEpochEarnSeconds)
    } else {
      //we are not updating anymore
      clearInterval(this.state.timer);
      this.setState({currentAmount : 0})
    }

  }

  calculateCurrentAmount = (startAmount, interestRate, startedAtEpochSecond) => {
    const averageEthBlockTimeSeconds = 15
    const calendarCommonYearSeconds = 31536000
    const timesEarnedPerYear = calendarCommonYearSeconds / averageEthBlockTimeSeconds
    const annualAddFactor = (1 + interestRate / timesEarnedPerYear) ** timesEarnedPerYear
    const amountAfterAnnum = startAmount * annualAddFactor
    const annualGains = amountAfterAnnum - startAmount
    const gainsPerBlock = (annualGains / (timesEarnedPerYear))
    const gainsPerSecond = gainsPerBlock / averageEthBlockTimeSeconds

    const currentSecond = this.nowSeconds()
    let currentSecondSinceStart = currentSecond - startedAtEpochSecond

    const timer = setInterval(() => {
      currentSecondSinceStart += 0.1 //100Ms
      const newAmount = (startAmount + (gainsPerSecond * currentSecondSinceStart))
      this.setState({ currentAmount: newAmount })
    }, 100)
    this.setState({timer})

  }


  //TODO make this a redirect instead of render if/else logic
  requestCreateNewLendingClub = () => {
    this.props.requestCreateNewLendingClub()
  }
  getDaiAndEthTestFunds = async () => {
    await this.props.getDaiAndEthTestFunds()
  }

  //TODO get amount from Input field
  payLendingClub = async () => {
    const { lendingClub } = this.props.lendingClubContractData
    const { contractParticipantsArray, amountPerPersonInDai, currentRound } = lendingClub
    const fundWholeRoundAmount = Math.round(contractParticipantsArray.length * amountPerPersonInDai)

    await this.props.payLendingClub(fundWholeRoundAmount)
    if (currentRound === 0 || currentRound >= contractParticipantsArray.length - 1) {
      //TODO fix manual update and let server fetch
      lendingClub.currentRound++
      if(currentRound === 0) lendingClub.startEpochEarnSeconds = this.nowSeconds()
      await this.initCurrentAmount(lendingClub)
    }
    }

  // weird quirk of the underlying api, have to use 3 separate functions...
  onViewTxTabClick = () => {
    this.setState({ currentTab: 'transactions' })
  }

  onViewDataClick = () => {
    this.setState({ currentTab: 'data' })
  }

  onViewFundClick = () => {
    this.setState({ currentTab: 'fund' })
  }

  TabDisplay = () => {
    const { fetchingPayLendingClub, fetchingFunds } = this.props
    const { lendingClub  } = this.props.lendingClubContractData
    const { txData } = lendingClub
    const { contractParticipants, contractParticipantsArray, currentRound, amountPerPersonInDai, lendingClubAddress, winners } = lendingClub
    const numContractParticipants = contractParticipantsArray.length
    const progress = Math.round((currentRound / numContractParticipants) * 100)
    const isLendingClubOver = progress >= 100
    const totalAmount = Math.round(numContractParticipants * amountPerPersonInDai)
    const { currentTab } = this.state

      switch (currentTab) {
        case 'data':
          return <Meta
            id="Meta-Data-1"
            className="Meta-Data"
            title="Your Lending Club"
            description={
              <div>
                <h4> ⏲️ Next Payout possible </h4>
                <p> now </p>
                {/* <h4> 👨 Amount per Person  </h4>
                <p> {amountPerPersonInDai}$ </p> */}
                <h4> 💰  Total amount to pay each round </h4>
                <p> {amountPerPersonInDai}$ x {numContractParticipants} participants = {totalAmount}$</p>
                <h4> 👨 Participants </h4>
                <p> {Object.keys(contractParticipants).join(', ')}</p>
                <h4> 🏆 Winners </h4>
                <p> {!winners ? 'no winners yet' : winners.join(", ") }</p>
                <h4> 📑 Deployed at address </h4>
                <p> {lendingClubAddress} </p>
                <h4> 💸 Current contract holdings </h4>
                <p> {this.state.currentAmount}</p>
              </div>
            }
          />
        case 'transactions':
          return <Meta
            className="third-data-step"
            title={<h3 style={{ marginBottom: '1rem' }}>Lending Club Transactions</h3>}
            description={
              <div>
                <Table pagination={{ position: 'top', pageSize: 2, showSizeChanger: false }} dataSource={txData} columns={columns} />
              </div>
            }
          />
        case 'fund':
          return <Meta
            title={<h3 style={{ marginBottom: '1rem' }}>Add funds to your Lending Club</h3>}
            description={
              <div>
                <Row.FixedHeight>
                  <Button
                    type="primary"
                    loading={fetchingFunds}
                    onClick={this.getDaiAndEthTestFunds}
                    icon="dollar"
                    >
                    Get Test Funds
                </Button>
                </Row.FixedHeight>
                {/* <Row > */}
                <Row.FixedHeight>

                  {
                    isLendingClubOver ? <p style={{ 'marginTop': '10px' }}> Your Lending Club has ended </p>
                      :
                      <Button
                        type="primary"
                        className="second-data-step"
                        loading={fetchingPayLendingClub}
                        onClick={this.payLendingClub}
                        icon="login"
                        >
                        Pay Lending Club
                      </Button>
                  }
                </Row.FixedHeight>
                <Row>
                  <Button
                    type="primary"
                    onClick={this.requestCreateNewLendingClub}
                    icon="reload"
                    >
                    Create New Lending Club
                </Button>
                </Row>
              </div>
            }
          />
      }
    }


  render = () => {
    const { lendingClub, user } = this.props.lendingClubContractData
    // const { name } = user
    const { contractParticipantsArray, currentRound } = lendingClub
    const progressTitle = currentRound === 0 ? 'no' : `${currentRound} payouts have happened so far`
    const numContractParticipants = contractParticipantsArray.length
    const progress = Math.round((currentRound / numContractParticipants) * 100)

    const TabDisplay = this.TabDisplay


    return (
      <>
        <BaseLayout>

          <StyledWrapper>
            <WalkThrough
              onViewTxTabClick={this.onViewTxTabClick}
              onViewDataClick={this.onViewDataClick}
              onViewFundClick={this.onViewFundClick}
            />
            <div className="card-wrapper">
            <Card.Styled
              className="first-data-step"
              cover={
                this.state.currentTab === 'data' ?
                  <Progress
                    className="fifth-data-step"
                    title={progressTitle}
                    type="circle"
                    strokeColor={{
                      '0%': '#108ee9',
                      '100%': '#87d068',
                    }}
                    percent={progress}
                  />
                  : <> </>
              }
              actions={[
                // <Button title="Create new Lending Club" onClick={this.createNewLendingClub}> <Icon type="edit" key="edit" />  </Button>,
                <Icon.Styled disabled key="transactions" title="View Transactions" onClick={this.onViewTxTabClick} type="unordered-list" />,
                <Icon.Styled key="data" title="Your Lending Club" onClick={this.onViewDataClick} type="team" />,
                <Icon.Styled key="fund" title="Fund Lending Club" onClick={this.onViewFundClick} type="login" />,
              ]}
            >
              <TabDisplay />
            </Card.Styled>
            </div>
          </StyledWrapper>
        </BaseLayout >
      </>
    )
  }
}
const reduxProps = ({ account, walkthrough }) => ({
  fetchingFunds: account.fetchingFunds,
  lendingClubContractData: account.lendingClubContractData,
  fetchingPayLendingClub: account.fetchingPayLendingClub,
  walkthroughRun: walkthrough.run,
  walkThroughStepIndex: walkthrough.stepIndex,
  stopWalkthrough: walkthrough.stopWalkthrough,
})

export default connect(
  reduxProps,
  { requestCreateNewLendingClub, getDaiAndEthTestFunds, payLendingClub },
)(LendingClubData);


