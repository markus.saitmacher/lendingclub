import account from './_account';
import metamask from './_metamask';
import notification from './_notification';
import tokenize from './_tokenize';
import walkthrough from './_walkthrough';
import warning from './_warning';
import {combineReducers} from 'redux';

export default combineReducers({
  account,
  metamask,
  notification,
  tokenize,
  warning,
  walkthrough,
});
