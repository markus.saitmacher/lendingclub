const express = require('express')
const path = require('path');
const contract = require('truffle-contract')
const assert = require('assert')
const fs = require('fs');
const cors = require('cors');
const shell = require('shelljs')
const bodyParser = require('body-parser')
const Web3 = require('web3');

const db = require('./db.js')
const utils = require('./utils.js')
const c = require('./constants')

const app = express()
const router = express.Router();
const web3 = new Web3(new Web3.providers.HttpProvider(c.HTTP_PROVIDER))
// const lendingClubArtifactsLocation = path.join(__dirname, '../client/src/contracts/LendingClub.json')
// const daiTokenArtifactsLocation = path.join(__dirname, '../client/src/contracts/DaiToken.json')
const lendingClubArtifactsLocation = path.join(__dirname, '../build/LendingClub.json')
const daiTokenArtifactsLocation = path.join(__dirname, '../build/DaiToken.json')
// const BN = web3.utils.BN;


//Middleware
app.use(cors())
app.use(bodyParser.json());
app.use('/', router); //define custom routes here if needed
//Once the sever starts up we deploy the smart contracts
const dataFromFrontend =
{
    "contractParticipants": {
        "main": "0x2bc5dac2080c6b0b5bc025e0b894d60fdb04a260",
        "hans": "0x015c53da62d4aedffe56187e401c6413cb07b444",
        "daniel": "0x50282cc400df9e89785d12a9413044ec627ba243"
    },
    "mainAccountAddress": "0x2bc5dac2080c6b0b5bc025e0b894d60fdb04a260",
    "amountPerPersonInDai": 10,
    "timeIntervalSeconds": 0
}
const testlendingClubContractData = {
    user:
    {
        userAddress: '0x81aeA7ABcf6fAFD7d263E4Adee24e31DC8FD92Cc',
        name: 'main',
        lendingClubAddress: '0x2BFE4B4Bd1cD117Bf126fAb12a0660376AdB0609',
        isMainOfCurrentLendingClub: true
    },
    lendingClub:
    {
        lendingClubAddress: '0x2BFE4B4Bd1cD117Bf126fAb12a0660376AdB0609',
        contractParticipants:
        {
            main: '0x81aeA7ABcf6fAFD7d263E4Adee24e31DC8FD92Cc',
            hans: '0x015c53da62d4aedffe56187e401c6413cb07b444',
            daniel: '0x50282cc400df9e89785d12a9413044ec627ba243'
        },
        contractParticipantsArray:
            ['0x81aeA7ABcf6fAFD7d263E4Adee24e31DC8FD92Cc',
                '0x015c53da62d4aedffe56187e401c6413cb07b444',
                '0x50282cc400df9e89785d12a9413044ec627ba243'],
        amountPerPersonInDai: 10,
        timeIntervalSeconds: 0,
        currentRound: 0
    }
}

router.get('/contracts/lendingclub.json', async (req, res) => {
    const lendingClubArtifacts = JSON.parse(fs.readFileSync(lendingClubArtifactsLocation))
    res.json(lendingClubArtifacts)
})

router.get('/contracts/daitoken.json', async (req, res) => {
    const daiTokenArtifacts = JSON.parse(fs.readFileSync(daiTokenArtifactsLocation))
    res.json(daiTokenArtifacts)
})






router.post('/createLendingClub', async (req, res) => {
    try {
        const { createLendingClubData }  = req.body
        if (createLendingClubData.testRun) {
            const lendingClubData = Object.assign({}, createLendingClubData)
            lendingClubData.lendingClubAddress = c.TEST_LENDING_CLUB_ADDRESS
            lendingClubData.currentRound = 0
            const performLendingClubCreatedMsg = await db.performLendingClubCreated(lendingClubData)
            if (!performLendingClubCreatedMsg.success) throw new Error(`Could not performLendingClubCreated, ${performLendingClubCreatedMsg.error}`)
            return res.json({ success: true, lendingClubAddress: c.TEST_LENDING_CLUB_ADDRESS })
        } else {
            const userAndLendingClubMsg = await db.getUserAndHisLendingClub(createLendingClubData.mainAccountAddress)
            if (userAndLendingClubMsg.success) {
                const updateUser = Object.assign({}, userAndLendingClubMsg.userAndLendingClubData.user)
                updateUser.lendingClubAddress = undefined //because we are updating
                const updateMsg = await db.addOrSetUser(updateUser)
                if (!updateMsg.success) throw new Error(`Could not update User, ${updateMsg.error}`)
            }

            //Returning early so connection wont timeout, client will fetch updated data
            res.json({ success: true })
            const ContractInteractionsInstance = await new ContractInteractions()
            await ContractInteractionsInstance.deployNewOrResetLendingClub(createLendingClubData)

        }
    } catch (error) {
        console.error('/createLendingClub', error)
        res.json({ success: false, error })
    }
})


//TODO figure out a better way for this
// and have if else for respondOnceTxsSettled with query tx
// return dai Tx hash and Eth tx hash
router.post('/getEthAndDaiTestFunds', async (req, res) => {
    try {
        const { userAddress, respondOnceTxsSettled } = req.body
        const accounts = await web3.eth.getAccounts()
        const mainAccount = accounts[0]
        const daiTokenArtifacts = JSON.parse(fs.readFileSync(daiTokenArtifactsLocation))
        const DaiTokenContract = contract(daiTokenArtifacts)
        DaiTokenContract.setProvider(web3.currentProvider)
        const dai = await DaiTokenContract.deployed()
        // const tokenDecimals = 18
        // const daiAmount = 100 * Math.pow(10, tokenDecimals)
        const daiAmount = web3.utils.toWei('100', 'ether') //works because token decimals also 18
        const daiTx = await utils.transferDaiToAddress(dai, userAddress, daiAmount, mainAccount)
        const ethAmount = web3.utils.toWei('1', 'ether');
        const ethTx = web3.eth.sendTransaction({ to: userAddress, from: mainAccount, value: ethAmount })
        console.log('Sent test funds')
        res.json({ success: true })

    } catch (error) {
        //TODO check for insufficient balance errors
        console.error('/getEthAndDaiTestFunds', error)
        res.json({ success: false, error: error.message })
    }



})

router.post('/getUserAndHisLendingClub', async (req, res) => {
    try {
        const { userAddress } = req.body
        const userAndLendingClubMsg = await db.getUserAndHisLendingClub(userAddress)
        if (userAndLendingClubMsg.success) {
            const { userAndLendingClubData } = userAndLendingClubMsg
            res.json({ success: true, userAndLendingClubData })
        } else {
            if (userAndLendingClubMsg.message) {
                res.json({ success: false, message: userAndLendingClubMsg.message })
            } else {
                throw new Error(userAndLendingClubMsg.message)
            }
        }
    } catch (error) {
        console.error('/getUserAndHisLendingClub', error)
        res.json({ success: false, error })
    }
})

// TODO:
// get current round to check that the payment was enough to increase round
// update user?
router.post('/apiUserPaidLendingClub', async (req, res) => {
    try {
        const { daiAmount, lendingClubAddress, txHash, winners } = req.body
        const txData = await web3.eth.getTransaction(txHash)
        txData.daiAmount = daiAmount
        const updatedLendingClubMsg = await db.updateLendingClubOnUserPayment(lendingClubAddress, txData)
        if (!updatedLendingClubMsg.success) {
            throw new Error(`Could not update User ${updatedLendingClubMsg.error}`)
        }
        res.json({ success: updatedLendingClubMsg.success, updatedLendingClub: updatedLendingClubMsg.updatedLendingClub })
    } catch (error) {
        console.error('/apiUserPaidLendingClub', error)
        res.json({ success: false, error })
    }
})



router.get('/healthcheck', (req, res) => {
    res.send('ok')
})

class ContractInteractions {
    constructor() {
        return (async () => {
            try {

                this.accounts = await web3.eth.getAccounts()
                this.account = this.accounts[0]
                this.rounds = this.accounts.length
                this.amount = 100
                this.timeIntervalSeconds = 0
                this.amountPerPerson = this.amount / this.accounts.length
                this.reallyHighAmount = 10000

                await this.migrateAndDeployContracts()

                return this
            } catch (error) {
                //This expected error should only occur on start so we migrate and deploy
                if (error.message.includes('LendingClub has not been deployed to detected network')) {
                    await this.migrateAndDeployContracts()
                    return this
                } else {
                    throw new Error(`Initialization ${error}`)
                }
            }
        })()
    }

    async migrateAndDeployContracts() {
        // cheesy workaround to not have to go to the pains of deploying manually
        shell.exec('npm run migrate:dev')
        await utils.sleepSeconds(c.SLEEP_DEPLOY_SECONDS) //sleep as this is synchronous command

        const lendingClubArtifacts = JSON.parse(fs.readFileSync(lendingClubArtifactsLocation))
        const daiTokenArtifacts = JSON.parse(fs.readFileSync(daiTokenArtifactsLocation))
        // assert(lendingClubArtifacts.address != this.lendingClubAddress || this.lendingClubAddress != undefined, `The Contract does not have a new address, deployment likely failed, new address: ${lendingClubArtifacts} , old address: ${this.lendingClubAddress} `)
        const LendingClubContract = contract(lendingClubArtifacts)
        const DaiTokenContract = contract(daiTokenArtifacts)
        LendingClubContract.setProvider(web3.currentProvider)
        DaiTokenContract.setProvider(web3.currentProvider)
        this.lc = await LendingClubContract.deployed()
        this.dai = await DaiTokenContract.deployed()
        this.daiAddress = this.dai.address
        this.lendingClubAddress = this.lc.address
        this.mainAddressDaiStartBalance = await utils.getDaiBalance(this.dai, this.account)
        this.subAddressDaiStartBalance = await utils.getDaiBalance(this.dai, this.accounts[1])
        return this.lendingClubAddress
    }

    async deployNewOrResetLendingClub(createLendingClubData) {
        try {
            const { contractParticipants, contractParticipantsArray, amountPerPersonInDai, timeIntervalSeconds, mainAccountAddress } = createLendingClubData
            const lendingClubAddress = this.lendingClubAddress
            const currentRound = 0

            //not main account because server is subsidizing deployment
            await utils.initializeLendingClub(this.lc, contractParticipantsArray, amountPerPersonInDai, timeIntervalSeconds, this.account)

            //Performing basic checks
            const amountPerPerson = await utils.getPublicVariableFromContract(this.lc, 'amountPerPerson')
            assert(amountPerPerson.toNumber() === amountPerPersonInDai, 'Does not match the amount per Person')

            // Allowing edge case for now
            // const currentContractDaiHoldings = await utils.getPublicVariableFromContract(this.lc, 'currentContractDaiHoldings')
            // assert(currentContractDaiHoldings.toNumber() === 0, 'Dai Holdings should be zero at start')
            const lendingClubData = {
                mainAccountAddress,
                lendingClubAddress,
                contractParticipants,
                contractParticipantsArray,
                amountPerPersonInDai,
                timeIntervalSeconds,
                currentRound,
            }
            const performLendingClubCreatedMsg = await db.performLendingClubCreated(lendingClubData)
            if (performLendingClubCreatedMsg.success) {
                console.log('Successfully initialized Lending Club')
                return lendingClubAddress
            } else {
                throw new Error(performLendingClubCreatedMsg.message)
            }
        } catch (e) {
            console.error('deployNewOrResetLendingClub error', e)
            throw new Error(`deployNewOrResetLendingClub ${e}`)
        }
    }

    async resetLendingClub() {
        //TODO assert that main Account is part of contractParticipants and we overwrite the lending club address for all the sub account as well
        // await this.migrateAndDeployContracts()
        //TODO expected case we create a new lending club for the user else
        // const userAndLendingClubMsg = await db.getUserAndHisLendingClub(mainAccountAddress)
        // if (!userAndLendingClubMsg.success) {
        // const lendingClubAddress = await this.migrateAndDeployContracts()
        // await this.migrateAndDeployContracts()
        // }
        // else {
        //TODO reset lending club should we send back all the dai holdings as well?
        // }

    }



}

//Handels unmatched routes, important that this comes at the end of the stack
app.use((_, res) => {
    res.json({ success: false, code: 404, error: 'Unsupported route' })
})

app.listen(c.PORT, () => console.log(`App listening on port ${c.PORT}!`))
