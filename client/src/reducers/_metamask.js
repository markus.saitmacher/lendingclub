import { apiGetMetamaskNetwork } from "../helpers/api";
import { parseError } from "../helpers/utilities";
import {
  accountUpdateAccountAddress,
  accountUpdateNetwork,
  accountUpdateWeb3,
  getUserAndHisLendingClub,
} from "./_account";
import { notificationShow } from "./_notification";
import Web3 from "web3";

// -- Constants ------------------------------------------------------------- //
const METAMASK_CONNECT_REQUEST = "metamask/METAMASK_CONNECT_REQUEST";
const METAMASK_CONNECT_SUCCESS = "metamask/METAMASK_CONNECT_SUCCESS";
const METAMASK_CONNECT_FAILURE = "metamask/METAMASK_CONNECT_FAILURE";
const METAMASK_NOT_AVAILABLE = "metamask/METAMASK_NOT_AVAILABLE";
const METAMASK_CLEAR_STATE = "metamask/METAMASK_CLEAR_STATE";

// -- Actions --------------------------------------------------------------- //

let accountInterval = null;

export const metamaskUpdateMetamaskAccount = () => async (dispatch, getState) => {
  const accounts = await window.web3.eth.getAccounts()
  const accountAddress = accounts[0]
  if (accountAddress !== getState().account.address) {
    dispatch(accountUpdateAccountAddress(accountAddress));
    dispatch(getUserAndHisLendingClub(accountAddress))
  }
}

export const metamaskConnectInit = () => async (dispatch, getState) => {
  try {
    //first we enable ethereum
    if (typeof window.ethereum !== "undefined") {
      await window.ethereum.enable();
    }
  } catch (error) {
    const message = parseError(error);
    dispatch(notificationShow(message, true));
    dispatch({ type: METAMASK_CONNECT_FAILURE });
  }

  //then we inject web3
  if (typeof window.web3 !== "undefined") {
    console.log("Browser has injected web3, so we are updating to >1.0")
    window.web3 = new Web3(window.web3.currentProvider);
    const accounts = await window.web3.eth.getAccounts()
    const accountAddress = accounts[0]
    if (!accountAddress) {
      dispatch(notificationShow("Unlock your Metamask", false));
    }
    dispatch(getUserAndHisLendingClub(accountAddress))
    dispatch(accountUpdateAccountAddress(accountAddress));
    dispatch({ type: METAMASK_CONNECT_REQUEST });
    try {
      const network = await apiGetMetamaskNetwork();
      dispatch({ type: METAMASK_CONNECT_SUCCESS, payload: network });
      dispatch(accountUpdateNetwork(network));
      dispatch(accountUpdateWeb3(window.web3));
      accountInterval = setInterval(
        () => dispatch(metamaskUpdateMetamaskAccount()),
        100
      );
    } catch (error) {
      const message = parseError(error);
      dispatch(notificationShow(message, true));
      dispatch({ type: METAMASK_CONNECT_FAILURE });
    }
  } else {
    dispatch(notificationShow("Install Metamask first", false));
    dispatch({ type: METAMASK_NOT_AVAILABLE });
  }
};

export const metamaskClearIntervals = () => dispatch => {
  clearInterval(accountInterval);
};

export const metamaskClearState = () => dispatch => {
  dispatch(metamaskClearIntervals());
  dispatch({ type: METAMASK_CLEAR_STATE });
};

// -- Reducer --------------------------------------------------------------- //
const INITIAL_STATE = {
  fetching: false,
  accountAddress: "",
  web3Available: false,
  network: "mainnet",
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case METAMASK_CONNECT_REQUEST:
      return {
        ...state,
        fetching: true,
        web3Available: false
      };
    case METAMASK_CONNECT_SUCCESS:
      return {
        ...state,
        fetching: false,
        web3Available: true,
        network: action.payload
      };
    case METAMASK_CONNECT_FAILURE:
      return {
        ...state,
        fetching: false,
        web3Available: true
      };
    case METAMASK_NOT_AVAILABLE:
      return {
        ...state,
        fetching: false,
        web3Available: false
      };
    case METAMASK_CLEAR_STATE:
      return {
        ...state,
        ...INITIAL_STATE
      };
    default:
      return state;
  }
};
