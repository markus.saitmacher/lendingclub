import React from 'react'
import { Row, Col, Tooltip, Icon, Input, Button, Checkbox, Tag } from 'antd';
import styled from 'styled-components'
import ChipInput from 'material-ui-chip-input'

const RowFixedHeight = styled(Row)`
    height: 75px !important;
`
const Box = styled(Row)`
    height: 15rem !important;
`
Row.Box = Box
Row.FixedHeight = RowFixedHeight

const Participants = ({
    nextStep,
    prevStep,
    handleAddParticipant,
    handleDeleteParticipant,
    participants,
    ethereumAddresses,
}) => {
    return (
        <>
           <Row style={{'height': '23rem' }}>
            {/* <Row> */}
                <Row.FixedHeight style={{'marginTop':'1rem'}}>
                    <Col>
                        {!participants.length
                            ? <h3 style={{ color: "white" }}>Enter your user name</h3>
                            : <h3 style={{ color: "white" }}>Now enter the names of all Participants</h3>
                        }
                        <ChipInput
                            style={{'marginTop': '40%'}}
                            fullWidth
                            defaultValue={['foo', 'bar']}
                            alwaysShowPlaceholder
                            placeholder="Type a name and hit enter"
                            value={participants}
                            onAdd={(participant) => handleAddParticipant(participant)}
                            onDelete={(participant, index) => handleDeleteParticipant(participant, index)}
                        />
                        {/* prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} */}
                    </Col>
                </Row.FixedHeight>
            </Row>
            {/* <Row.FixedHeight>
                <Col>
                    <div className="tags" style={{ "maxWidth": "300px" }}>
                        {
                            participants.map((participant, index) => {
                                const slicedParticipant = participant.slice(0, 20)
                                return (
                                    //TODO make first one adjustable with name
                                    <Input
                                        value={ethereumAddresses[participant]}
                                        placeholder="Enter the Ethereum address"
                                        addonBefore={slicedParticipant}
                                        // prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        // prefix={
                                        //         <Tag color={"blue"} key={index}>
                                        //             {slicedParticipant}
                                        //         </Tag>
                                        // }
                                        suffix={
                                            <Tooltip title="Provide your own or take the prepopulated Ethereum address">
                                                <Icon type="info-circle" style={{ color: 'rgba(0,0,0,.45)' }} />
                                            </Tooltip>
                                        }
                                    />
                                )
                            })
                        }
                    </div>
                </Col>
            </Row.FixedHeight> */}
            <Row.FixedHeight >
                <Col>
                    <Button disabled type="primary" onClick={prevStep} style={{ marginRight: '2rem' }}>  <Icon type="left" /> </Button>
                    <Button className="first-create-step" disabled={participants.length ? false : true} type="primary" onClick={nextStep}>  <Icon type="right" /> </Button>
                </Col>
            </Row.FixedHeight>
        </>
    )
}
// const WrappedParticipants = Form.create({ name: 'participants' })(Participants);
export default Participants