import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Steps, Icon } from 'antd'
import Participants from '../components/LendingInputs/1_Participants'
import EthAddresses from '../components/LendingInputs/2_EthAddresses'
import Frequency from '../components/LendingInputs/3_Frequency'
import AmountUSD from '../components/LendingInputs/4_AmountUSD'
import Create from '../components/LendingInputs/5_Create'
import WalkThrough from '../components/WalkThrough'
import BaseLayout from '../layouts/base';
import styled from 'styled-components';
import NotFound from './NotFound';
import { createLendingClub } from '../reducers/_account';
import Web3 from 'web3';
const httpProvider = 'https://kovan.infura.io/v3/ffa771bbbe4344ed88d8dd38d8cf3f55'
const web3 = new Web3(new Web3.providers.HttpProvider(httpProvider))

const { Step } = Steps
const StyledWrapper = styled.div`
  width: 300px;
  margin-top: 25%;
  display: flex;
  flex-direction: column;
  padding: 20px;
  text-align: center;
`;

const StyledDomains = styled.div`
  width: 100%;
  height: 100%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

class LendingClubCreate extends Component {
    state = {
        step: 1,
        participants: [],
        participantInputValue: "",
        ethereumAddresses: {},
        amountPerPersonInDai : 100,
        timeIntervalSeconds : 0,
        show: 'no',
        daysIntervalTime: 0,
        hoursIntervalTime: 0,
        minutesIntervalTime: 0,
    }
    componentDidMount(){
        this.handleAddParticipant('YourUserName')
        if(this.props.walkthroughRun) {
            this.handleAddParticipant('YourUserName')
        }
    }

    nextStep = (calledFrom) => {
        if(this.props.walkthroughRun && calledFrom !== 'tour') return //workaround for tour
        const { step } = this.state
        this.setState({
            step: step + 1
        })
    }

    prevStep = (calledFrom) => {
        if(this.props.walkthroughRun && calledFrom !== 'tour') return //workaround for tour
        // e.preventDefault(); TODO do we need this?
        const { step } = this.state
        this.setState({
            step: step - 1
        })
    }

    handleChange = input => event => {
        event.preventDefault()
        this.setState({ [input]: event.target.value })
    }


    callCreateLendingClub = async () => {
        //making sure mainAccountAddress in
        const testRun = this.props.walkthroughRun
        const { ethereumAddresses, participants, amountPerPersonInDai } = this.state
        const mainAccountAddress =  ethereumAddresses[participants[0]]
        const contractParticipants = ethereumAddresses
        const contractParticipantsArray = Object.values(ethereumAddresses)
        const timeIntervalSeconds = 0 //TODO setting this to 0 for now
        const createContractData = {
            contractParticipants,
            contractParticipantsArray,
            timeIntervalSeconds,
            mainAccountAddress,
            amountPerPersonInDai,
            testRun,
        }
        await this.props.createLendingClub(createContractData)
    }

    //Not ment for prod, for dev account cold be saved on the server
    //TODO disable with env STAGE var and use real web3 for testnet  const { web3 } = this.props
    genRandomAccountAddress = () => {
        //  if(this.props.walkthroughRun) return '0xA0283F38567B0692A910137161524dD6b333A8e7'
        const randomAccount = web3.eth.accounts.create();
        return randomAccount.address
    }

    handleAddParticipant = (participant) => {
        const isFirstParticipant = this.state.participants.length === 0 && !this.props.walkthroughRun
        const ethereumAddress = isFirstParticipant ? this.props.mainAccountAddress : this.genRandomAccountAddress()
        this.setState(
            {
                participants: [...this.state.participants, participant],
                ethereumAddresses: { ...this.state.ethereumAddresses, [participant]: ethereumAddress }
            })
    }

    handleDeleteParticipant = (_, index) => {
        this.setState(state => ({ participants: state.participants.filter((_, i) => i !== index) }))
    }

    handleChangeParticipantEthAddress = e => {
        const participant = e.target.name
        const ethereumAddress = e.target.value
        this.setState({ethereumAddresses: { ...this.state.ethereumAddresses, [participant]: ethereumAddress } })
    }

    onDaiAmountChange = value => this.setState({ amountPerPersonInDai: value })

    onIntervalChange = (value, intervalType) => {
        if (!isNaN(value)) {
            this.setState({ [intervalType]: value })
        }
    }

    handleShowChange = e => {
        this.setState({ show: e.target.value });
    }

    // render() {
        // const StepForm = () => {
    StepForm = () => {
            const { step } = this.state
            switch (step) {
                case 1:
                    return <Participants
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        participants={this.state.participants}
                        handleAddParticipant={this.handleAddParticipant}
                        handleDeleteParticipant={this.handleDeleteParticipant}
                        ethereumAddresses={this.state.ethereumAddresses}
                    />
                case 2:
                    return <EthAddresses
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        participants={this.state.participants}
                        ethereumAddresses={this.state.ethereumAddresses}
                        handleChangeParticipantEthAddress={this.handleChangeParticipantEthAddress}
                    />
                case 3:
                    return <Frequency
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange={this.handleChange}
                        show={this.state.show}
                        daysIntervalTime={this.state.daysIntervalTime}
                        hoursIntervalTime={this.state.hoursIntervalTime}
                        minutesIntervalTime={this.state.minutesIntervalTime}
                        handleShowChange={this.handleShowChange}
                        onIntervalChange={this.onIntervalChange}

                    />
                case 4:
                    return <AmountUSD
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        amountPerPersonInDai={this.state.amountPerPersonInDai}
                        onDaiAmountChange={this.onDaiAmountChange}
                    />
                case 5:
                    return <Create
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        handleChange={this.handleChange}
                        createIconLoading={this.state.createIconLoading}
                        callCreateLendingClub={this.callCreateLendingClub}
                        fetchingCreateLendingClub={this.props.fetchingCreateLendingClub}
                        createData={this.state}
                    />
                default:
                    return <NotFound />

            }
        }
    render() {
        const StepForm = this.StepForm

        return (
            <>
                <BaseLayout>
                    <WalkThrough
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        callCreateLendingClub={this.callCreateLendingClub}
                        createStep={this.state.step}
                    />
                    <StyledWrapper>
                        <StepForm />
                    </StyledWrapper>
                </BaseLayout>
            </>
        )
    }
}
const reduxProps = ({ account, walkthrough }) => ({
    web3: account.web3,
    lendingClubContractData: account.lendingClubContractData,
    fetching: account.fetching,
    fetchingCreateLendingClub: account.fetchingCreateLendingClub,
    mainAccountAddress: account.address,
    walkthroughRun: walkthrough.run,
});

export default connect(
    reduxProps,
    { createLendingClub }
)(LendingClubCreate);


