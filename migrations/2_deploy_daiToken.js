// We are deploying the underlying ERC20 first and then we pass
// it's address to the LendingClub constructor
const DaiToken = artifacts.require('./DaiToken.sol')

module.exports = function (deployer, network, accounts) {
    try {
        //Really cheesy workaround to only deploy daitoken once,
        //we let this fail and then deploy, if it has an address we dont deploy
        console.log('DaiToken.address', DaiToken.address)
    } catch (e) {
        deployer.deploy(DaiToken);
    }
}
