// We are deploying the underlying ERC20 first and then we pass
// it's address to the LendingClub constructor
const LendingClub = artifacts.require("./LendingClub.sol")
const DaiToken = artifacts.require('./DaiToken.sol')

module.exports = function (deployer) {
  console.log('DaiToken.address', DaiToken.address)
  deployer.deploy(LendingClub, DaiToken.address)
}
