// exec ./scripts/truffle_script.js
const contract = require('truffle-contract')
const assert = require('assert')
const lendingClubArtifacts = require('../client/src/contracts/LendingClub.json')
const daiTokenArtifacts = require('../client/src/contracts/DaiToken.json')
const utils = require('../server/utils.js')
const BN = web3.utils.BN;
//get the contract instances and inject web3
const LendingClubContract = contract(lendingClubArtifacts)
const DaiTokenContract = contract(daiTokenArtifacts)
LendingClubContract.setProvider(web3.currentProvider)
DaiTokenContract.setProvider(web3.currentProvider)


class Tests {

    constructor() {
        return (async () => {
            try {
                this.accounts = await web3.eth.getAccounts()
                this.account = this.accounts[0]
                this.rounds = this.accounts.length
                this.amount = 100
                this.timeIntervalSeconds = 0
                this.amountPerPerson = this.amount / this.accounts.length
                this.reallyHighAmount = 10000
                this.lc = await LendingClubContract.deployed()
                this.dai = await DaiTokenContract.deployed()
                this.daiAddress = this.dai.address
                this.lendingClubAddress = this.lc.address
                this.mainAddressDaiStartBalance = await utils.getDaiBalance(this.dai, this.account)
                this.subAddressDaiStartBalance = await utils.getDaiBalance(this.dai, this.accounts[1])
                return this
            } catch(e) {
                console.error('initialization failed', e)
                throw new Error(e)
            }
        })()
    }

    async scenarioFullLendingCircle() {
        try {
            await this.testLendingClubInitialization()

            await this.testAllowance()

            await this.testFullPaymentLoop()

            await this.testFailureAfterLendingRoundEnded()

            console.log('Finished scenarioFullLendingCircle successfully')

        } catch (e) {
            console.error(e)
            throw new Error(`Test Error ${e}`)
        }
    }

    async scenarioTimingWorks() {
        try {

            this.timeIntervalSeconds = 10
            await this.testLendingClubInitialization()

            await this.testAllowance()

            let nextPayoutPossibleAtSecond = await utils.getPublicVariableFromContract(this.lc, 'nextPayoutPossibleAtSecond')
            let secondsSinceEpoch = Math.round(Date.now() / 1000)
            assert(nextPayoutPossibleAtSecond.toNumber() > secondsSinceEpoch, `Does not lay in the past, first check, timeInContract: ${nextPayoutPossibleAtSecond.toNumber()}, now: ${secondsSinceEpoch} `)

            const error = await utils.depositWithExpectedFailure(this.lc, this.amount, this.account)
            assert(error.reason.includes('The contract can not be paid yet'), `Unexpected Error message or no error: ${error}`)


            await utils.sleepSeconds(this.timeIntervalSeconds);

            nextPayoutPossibleAtSecond = await utils.getPublicVariableFromContract(this.lc, 'nextPayoutPossibleAtSecond')
            secondsSinceEpoch = Math.round(Date.now() / 1000)
            assert(nextPayoutPossibleAtSecond.toNumber() <= secondsSinceEpoch, 'We did not sleep enough')

            const successfulTx = await utils.depositDaiToLendingClub(this.lc, this.amount, this.account)
            assert(successfulTx.receipt.status, `Tx was not successfull`)

            //crucial now testing that we cannot send tx
            nextPayoutPossibleAtSecond = await utils.getPublicVariableFromContract(this.lc, 'nextPayoutPossibleAtSecond')
            secondsSinceEpoch = Math.round(Date.now() / 1000)
            assert(nextPayoutPossibleAtSecond.toNumber() > secondsSinceEpoch, 'Does not lay in the past, second check')

            //Assert failure
            const error2 = await utils.depositWithExpectedFailure(this.lc, this.amount, this.account)
            assert(error2.reason.includes('The contract can not be paid yet'), `Unexpected Error message or no error: ${error2}`)

            await utils.sleepSeconds(this.timeIntervalSeconds);

            //TODO assert no failure and check balance
            const successfulTx2 = await utils.depositDaiToLendingClub(this.lc, this.amount, this.account)
            assert(successfulTx2.receipt.status, `Tx2 was not successfull`)

            //Cleaning up test
            //resetting for latter tests
            await this.testCleanUp()
            this.timeIntervalSeconds = 0


            console.log('Finished scenarioTimingWorks successfully')
        } catch (e) {
            console.error(e)
            throw new Error(`ScenarioTimingWorks Error ${e}`)
        }
    }

    async testCleanUp() {

        await utils.withdrawContractDaiBalanceToOwner(this.lc, this.account)
        let contractDaiBalance = await utils.getDaiBalance(this.dai, this.lendingClubAddress)
        assert(contractDaiBalance.toNumber() === 0, `Contract still has some founds, got: ${contractDaiBalance.toNumber()}`)

        //now transferring all the sub balances to the main account
        for(let i=1; i < this.accounts.length; i++) {
            const account = this.accounts[i]
            const accountBalance = await utils.getDaiBalance(this.dai, account)
            await utils.transferDaiToAddress(this.dai, this.account, accountBalance, account)
        }
        const finalAccountBalance = await utils.getDaiBalance(this.dai, this.accounts[this.accounts.length - 1])
        assert(finalAccountBalance == 0, `Final account Balance is not 0, but ${finalAccountBalance}`)
        this.subAddressDaiStartBalance = 0
        this.mainAddressDaiStartBalance = await utils.getDaiBalance(this.dai, this.account)
    }


    async testLendingClubInitialization() {
        try {
            //making sure we can actually run this
            const potentialWinnersCount = await utils.getPublicVariableFromContract(this.lc, 'getPotentialWinnersCount')
            const currentRound = await utils.getPublicVariableFromContract(this.lc, 'currentRound')
            if (potentialWinnersCount.toNumber() === this.accounts.length || currentRound.toNumber() > 0) {
                await this.testCleanUp()
                await utils.resetLendingClubContract(this.lc, this.account)
            }


            await utils.initializeLendingClub(this.lc, this.accounts, this.amountPerPerson, this.timeIntervalSeconds, this.account)

            const amountPerPerson = await utils.getPublicVariableFromContract(this.lc, 'amountPerPerson')
            assert(amountPerPerson.toNumber() === this.amountPerPerson, 'Does not match the amount per Person')
            const timeIntervalSeconds = await utils.getPublicVariableFromContract(this.lc, 'timeIntervalSeconds')
            assert(timeIntervalSeconds.toNumber() === this.timeIntervalSeconds, 'Does not match the assigned interval seconds')
            const totalLendingAmountPerRound = await utils.getPublicVariableFromContract(this.lc, 'totalLendingAmountPerRound')
            assert(totalLendingAmountPerRound.toNumber() === this.amount, 'Does not match the assigned amount')
            const currentRound2 = await utils.getPublicVariableFromContract(this.lc, 'currentRound')
            assert(currentRound2.toNumber() === 0, `No Round should have started yet, round: ${currentRound2.toNumber()} `)
            const currentContractDaiHodlings = await utils.getPublicVariableFromContract(this.lc, 'currentContractDaiHoldings')
            assert(currentContractDaiHodlings.toNumber() === 0, 'Dai Holdings should be Zero at this Point')
            // const nextPayoutPossibleAtSecond = await utils.getPublicVariableFromContract(this.lc, 'nextPayoutPossibleAtSecond')
            // assert(nextPayoutPossibleAtSecond.toNumber() < Date.now() + this.timeIntervalSeconds, 'Does not lay in the past')
            const potentialWinnersCount2 = await utils.getPublicVariableFromContract(this.lc, 'getPotentialWinnersCount')
            const contractParticipantsCount = await utils.getPublicVariableFromContract(this.lc, 'getContractParticipantsCount')
            assert(contractParticipantsCount.toNumber() === potentialWinnersCount2.toNumber(), 'Potential winners should match contractParticipants at start')

        } catch (e) {
            console.error(e)
            throw new Error(`TestLendingClubInitialization Error ${e}`)
        }
    }


    async testAllowance() {
        await utils.approveAddressForDaiSpending(this.dai, this.lendingClubAddress, this.reallyHighAmount, this.account)
        const allowance = await utils.getDaiAllowanceForAddress(this.dai, this.account, this.lendingClubAddress)
        assert(allowance.toNumber() === this.reallyHighAmount)
    }

    async testFailureAfterLendingRoundEnded() {
        const potentialWinnersCount = await utils.getPublicVariableFromContract(this.lc, 'getPotentialWinnersCount')
        assert(potentialWinnersCount.toNumber() === 0, `The current round has not ended yet ${potentialWinnersCount.toNumber()} `)

        const error = await utils.depositWithExpectedFailure(this.lc, this.amount, this.account)
        //TODO make this an error code in the contract
        assert(error.reason.includes('Everyone has already been payed out'), 'Did not throw an expected error when lending round ended')
    }


    // paying for everyone from main account and then make 2 payments for the last round
    async testFullPaymentLoop() {
        try {

            while (await utils.getPublicVariableFromContract(this.lc, 'currentRound') < this.rounds - 1) {
                await utils.depositDaiToLendingClub(this.lc, this.amount, this.account)
            }

            //Now testing that sending partial amounts also works
            //first half
            const halfAmount = Math.ceil(this.amount / 2)
            await utils.depositDaiToLendingClub(this.lc, halfAmount, this.account)
            //TODO assert the contract balance here to be halfAmount
            //second half
            await utils.depositDaiToLendingClub(this.lc, halfAmount, this.account)


            let contractDaiBalance = await utils.getDaiBalance(this.dai, this.lendingClubAddress)
            assert(contractDaiBalance.toNumber() === 0, `Contract still has some founds, got: ${contractDaiBalance.toNumber()}`)
            // assuming sorted array
            let daiBalances = await utils.getAllDaiBalances(this.dai, this.accounts)
            // daiBalances.forEach(balance => console.log('balance', balance.toString())) //printing balances for manual tests
            for (let i = 0; i < daiBalances.length; i++) {
                const balance = daiBalances[i]
                if (i === 0) {
                    //TODO use Bignumberjs to assert here
                    const payouts = daiBalances.length - 1
                    const payoutsAmount = new BN(payouts * this.amount)
                    const mainAddressExpectedBalance = this.mainAddressDaiStartBalance.sub(payoutsAmount)
                    assert(balance.toString() === mainAddressExpectedBalance.toString(), 'Main account balance is incorrect')
                } else {
                    // const expectedBalance = this.subAddressDaiStartBalance.add(new BN(this.amount))
                    const expectedBalance = this.amount
                    assert(balance.toString() === expectedBalance.toString(), `Sub account balance is incorrect, got: ${expectedBalance.toString()} , expected:${balance.toString()}`)
                }
            }
        } catch (e) {
            console.error(e)
            throw new Error(`FullPaymentLoop ${e}`)
        }
    }

}


module.exports = async (config) => {
    try {
        console.log('starting')
        const TestsInstance = await new Tests()
        console.log('running scenarioFullLendingCircle')
        await TestsInstance.scenarioFullLendingCircle()

        await TestsInstance.scenarioTimingWorks()

        await TestsInstance.scenarioFullLendingCircle()

        console.log('All tests finished successfully')
    } catch (e) {
        // console.log('Script Error', e)
        throw new Error(`Script Error ${e}`)
    }
}

// config_[0] is the command name, e.g. 'hello' here.
//   let name = config._.length > 1 ? config._[1] : 'World!';
