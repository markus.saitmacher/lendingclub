import React from 'react';
import ReactDOM from 'react-dom';
import { injectGlobal } from 'styled-components';
import { globalStyles } from './styles';
import Root from './Root';
// import * as serviceWorker from './serviceWorker';

// eslint-disable-next-line
injectGlobal`${globalStyles}`;

ReactDOM.render(<Root />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();

// import { injectGlobal } from 'styled-components';
// import { createGlobalStyle } from 'styled-components';
// import { globalStyles } from './styles';
// const GlobalStyle = createGlobalStyle`
//     ${globalStyles()}
// `;
// eslint-disable-next-line
// injectGlobal`${globalStyles}`;
// ReactDOM.render(
//     <React.Fragment>
//         <GlobalStyle />
//         <Root />
//     </React.Fragment>,
//         document.getElementById('root'));
