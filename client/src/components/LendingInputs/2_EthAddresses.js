import React from 'react'
import { Row, Col, Tooltip, Icon, Input, Button, Checkbox, Tag } from 'antd';
import styled from 'styled-components'

const RowFixedHeight = styled(Row)`
    height: 75px !important;
`
const Box = styled(Row)`
    height: 15rem !important;
`
Row.Box = Box
Row.FixedHeight = RowFixedHeight

const EthAddresses = ({
    nextStep,
    prevStep,
    participants,
    ethereumAddresses,
    handleChangeParticipantEthAddress,
}) => {
    return (
        <>
           <Row style={{'height': '23rem' }}>
            {/* <Row.FixedHeight> */}
                <h3 style={{ color: "white" }}> Ethereum Addresses for all participants </h3>
             <Row.FixedHeight style={{'marginTop':'1rem'}}>
                <Col>
                    <div className="tags" style={{ "maxWidth": "300px" }}>
                        {
                            participants.map((participant, index) => {
                                const slicedParticipant = participant.slice(0, 20)
                                return (
                                    //TODO make first one adjustable with name
                                    <Input
                                        style={{'marginTop': '1rem'}}
                                        key={participant}
                                        name={participant}
                                        value={ethereumAddresses[participant]}
                                        onChange={handleChangeParticipantEthAddress}
                                        // onChange={(e, participant) => handleChangeParticipantEthAddress(e, participant)}
                                        placeholder="Enter the Ethereum address"
                                        addonBefore={slicedParticipant}
                                        // prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        // prefix={
                                        //         <Tag color={"blue"} key={index}>
                                        //             {slicedParticipant}
                                        //         </Tag>
                                        // }
                                        suffix={
                                            <Tooltip title="Provide your own or take the pre populated Ethereum address">
                                                <Icon type="info-circle" style={{ color: 'rgba(0,0,0,.45)' }} />
                                            </Tooltip>
                                        }
                                    />
                                )
                            })
                        }
                    </div>
                </Col>
                </Row.FixedHeight>
            </Row>
            <Row.FixedHeight>
                <Col>
                    <Button type="primary" onClick={prevStep} style={{ marginRight: '2rem' }}>  <Icon type="left" /> </Button>
                    <Button className="second-create-step" type="primary" onClick={nextStep}>  <Icon type="right" /> </Button>
                    {/* <Button className="second-create-step" type="primary" onClick={nextStep}>  <Icon type="right" /> </Button> */}
                </Col>
            </Row.FixedHeight>
        </>
    )
}
export default EthAddresses
