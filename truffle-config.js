const HDWalletProvider = require('truffle-hdwallet-provider')
const path = require('path');
const mnemonic = 'arctic cotton uniform vast sock dizzy sell poem bread govern feed visa'
const walletChildNum = 0;
const kovanNetworkAddress = 'https://kovan.infura.io/v3/ffa771bbbe4344ed88d8dd38d8cf3f55'
const mainnetNetworkAddress = '';

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
//  contracts_build_directory: path.join(__dirname, 'client/src/contracts'),
 contracts_build_directory: path.join(__dirname, './build'),
  networks: {
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*',
      gas: 6600000,
    },
    kovan: {
      network_id: 42,
      provider: function() {
        return new HDWalletProvider(mnemonic, kovanNetworkAddress, walletChildNum)
      },
      // kovan block limit
      gas: 4712388,
     // gas: 10000000,
      // gasPrice: 0x01
    },
    mainnet: {
      network_id: 1,
      provider: function () {
        return new HDWalletProvider(mnemonic, mainnetNetworkAddress, walletChildNum)
      },
    },
  },
  compilers: {
    solc: {
      version: '0.5.3',
      // version: '0.5.8',
      settings: {
        optimizer: {
          enabled: true,
          runs: 200
        }
      }
    }
  }
}
// module.exports = {
//     networks: {
//         development: {
//             host: 'localhost',
//             port: 8545,
//             network_id: '*',
//             gas: 6000000
//         },
//         // Ropsten testnet
//         ropsten: {
//             confirmations: 2, // # of confs to wait between deployments. (default: 0)
//             skipDryRun: true,
//             provider:  () => new HDWalletProvider(mnemonic, nodeApiUrl),
//             network_id: '3',
//             gas: 6000000,
//             gasPrice: 10000000000 // 10 Gwei
//         },
//     },
//     compilers: {
//         solc: {
//             version: '0.5.3',
//             settings: {
//                 optimizer: {
//                     enabled: true,
//                     runs: 200
//                 }
//             }
//         }
//     }
// }
