pragma solidity 0.5.3;

import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20Detailed.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20Capped.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/ownership/Ownable.sol";

/**
 * @dev Example of the ERC20 Token.
 */
contract DaiToken is Ownable, ERC20Detailed, ERC20Capped {

	using SafeMath for uint256;

	uint256 CAP = 1000000000;
	uint256 TOTALSUPPLY = CAP.mul(10 ** 18);

	constructor()
		public
		Ownable()
	{
    	ERC20Detailed.initialize('DaiToken', 'DAI', 18);
		ERC20Capped.initialize(TOTALSUPPLY, msg.sender); //or address(this) ???
		_mint(msg.sender, TOTALSUPPLY);
	}
}
