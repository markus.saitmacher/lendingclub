import React, { Component } from "react"
import PropTypes from "prop-types"
import styled from "styled-components"
import { connect } from "react-redux"
import BaseLayout from "../layouts/base"
import Button from "../components/Button"
import Column from "../components/Column"
import { initializeWalkthrough, stopWalkthrough} from "../reducers/_walkthrough"
import { metamaskConnectInit } from "../reducers/_metamask"
import { fonts } from "../styles"
import { Link } from 'react-router-dom'

const StyledLanding = styled.div`
  width: 100%
  height: 600px
  display: flex
  flex-direction: column
  justify-content: center
  align-items: center
`

const StyledButtonContainer = styled(Column)`
  width: 200px
  margin: 50px 0
`

const StyledConnectButton = styled(Button)`
  background: ${props => props.colorStyle}
  border-radius: 8px
  font-size: ${fonts.size.medium}
  height: 44px
  width: 100%
  margin: 12px 0
`
const StyledHeader = styled.h1`
  font-size: 42px;
  color: #fff;
`


class Home extends Component {
  state = {
    run: false
  }

  componentDidMount(){
    this.props.stopWalkthrough()
  }

  initWalkthrough = e  => {
    e.preventDefault()

    this.setState({
      run: true
    })

  }

  render(){
    const { run } = this.state

    return (
    <BaseLayout>
      <StyledLanding>
        <StyledHeader>
          Create a Lending Club with your friends
          <br />
          and earn interest together 💸
        </StyledHeader>
        <StyledButtonContainer>
          <StyledConnectButton
            left
            color="orange"
            onClick={this.props.metamaskConnectInit}
          >
            {"Connect to Metamask"}
          </StyledConnectButton>
         <Link to="/create">
          <StyledConnectButton
            left
            style={{'width': '200px'}}
            colorStyle="#1890ff"
            onClick={this.props.initializeWalkthrough}
          >
            {"Tour"}
          </StyledConnectButton>
        </Link>
        </StyledButtonContainer>
      </StyledLanding>
    </BaseLayout>
  )
      }
}

Home.propTypes = {
  metamaskConnectInit: PropTypes.func.isRequired,
  initializeWalkthrough: PropTypes.func.isRequired,
}

export default connect(
  null,
  {
    metamaskConnectInit,
    initializeWalkthrough, stopWalkthrough
  }
)(Home)

