import React, { Component } from 'react'
// import { Form, Button } from 'antd'
import { Tree, TreeSelect, Row, Col, InputNumber, Button, Icon }  from 'antd';
import styled from 'styled-components'


const RowFixedHeight = styled(Row)`
    height: 75px !important;
`
const Box = styled(Row)`
    height: 15rem !important;
`
Row.Box = Box
Row.FixedHeight = RowFixedHeight

const treeData = [
  {
    title: 'Node1',
    value: '0-0',
    key: '0-0',
    children: [
      {
        title: 'Child Node1',
        value: '0-0-1',
        key: '0-0-1',
      },
      {
        title: 'Child Node2',
        value: '0-0-2',
        key: '0-0-2',
      },
    ],
  },
  {
    title: 'Node2',
    value: '0-1',
    key: '0-1',
  },
];

class Create extends Component {
    state = {
        amount : 0,
    }

    // onChange = e => this.setState({ [e.target.name]: e.target.value })

    render() {
        return (
            <>
           <Row style={{'height': '23rem' }}>
                <Row.FixedHeight>
                    <h3 style={{ color: "white" }}>Create your Lending Club now!</h3>
                </Row.FixedHeight>

                {/* TODO show details of data here like https://tlc-testnet.wetrust.io/roscas/new */}
                 <Row.FixedHeight style={{'marginTop':'6rem'}}>
                    <Col>
                    <Button
                        className="fifth-create-step"
                        type="primary"
                        loading={this.props.fetchingCreateLendingClub}
                        onClick={this.props.callCreateLendingClub}
                        >
                    Create
                    </Button>
                    <p style={{marginTop: '10px'}}> This will deploy your contract on the blockchain and may take up to a minute</p>
                    </Col>
                </Row.FixedHeight>
                   {/* <Tree
                    style={{ width: '100%' }}
                    // value={this.state.value}
                    dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                    // treeData={treeData}
                    treeData={[this.props.createData]}
                    placeholder="Please select"
                    treeDefaultExpandAll
                    // onChange={this.onChange}
                    /> */}
                </Row>
                <Row.FixedHeight>
                    <Col>
                        <Button type="primary" onClick={this.props.prevStep} style={{ marginRight: '2rem' }}>  <Icon type="left" /> </Button>
                        <Button disabled type="primary" onClick={this.props.nextStep}>  <Icon type="right" /> </Button>
                    </Col>
                </Row.FixedHeight>
            </>
        )
    }
}
export default Create

