// -- Constants ------------------------------------------------------------- //
const INITIALIZE_WALKTHROUGH = 'walkthrough/INITIALIZE_WALKTHROUGH'
const STOP_WALKTHROUGH = 'walkthrough/STOP_WALKTHROUGH'
const UPDATE_STEP_INDEX = 'walkthrough/UPDATE_STEP_INDEX'

// -- Actions --------------------------------------------------------------- //

export const updateWalkthroughStepIndex = (stepIndex) => dispatch => {
    dispatch({ type: UPDATE_STEP_INDEX, payload: stepIndex })
}

export const initializeWalkthrough = () => dispatch => {
    dispatch({ type: INITIALIZE_WALKTHROUGH})
}

export const stopWalkthrough = () => dispatch => {
    dispatch({ type: STOP_WALKTHROUGH })
}

// -- Reducer --------------------------------------------------------------- //
const INITIAL_STATE = {
    stepIndex: 0,
    run: false
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case INITIALIZE_WALKTHROUGH:
        return {...state, run: true}
    case STOP_WALKTHROUGH:
        return {...state, run: false}
    case UPDATE_STEP_INDEX:
        return {...state, stepIndex: action.payload}
    default:
      return state;
  }
};
