# LendingClubDapp - [http://lendingclubdapp.com](http://lendingclubdapp.com)
---

![App](client/src/assets/lendingcubdapp.png)


This is an implementation of a Lending Club, sometimes called savings circle or ROSCA on the ethereum blockchain. It lets you gather funds with your friends to save for expensive purchases by paying into a pool regularly and alternating who receives the pool. For example, in a group of 12 members with a contribution of $100 per month, when it is a member's turn to receive the funds (randomly determined), they receive a sum of $1200.

By using Dai and
[Compound Finance](https://compound.finance/)
3$ to 10$ interest is earned on the funds in the pool.

# Installation

Install server and client dependencies
```
npm run install:all
```

Run all services (blockchain, server and client)

```
npm run start:dev
```
or run them separately
```
npm run blockchain
npm run server
npm run client
```

To gain an even further understanding of the app, start the Tour of the Dapp on the Home page.

Connect to your local ganache blockchain or any other provider via Metamask to interact with the smart contract directly.

See below on how to connect to my blockchain instance at
http://blockchain.lendingclubdapp.com
do the following:


![Meta Mask add RPC demo](client/src/assets/Metamask.gif)