import React, { Component } from 'react'
// import { Form, Button } from 'antd'
import { Row, Col, InputNumber, Button, Icon } from 'antd';
import styled from 'styled-components'


const RowFixedHeight = styled(Row)`
    height: 75px !important;
`
Row.FixedHeight = RowFixedHeight
const Box = styled(Row)`
    height: 15rem !important;
`
Row.Box = Box


class AmountUSD extends Component {
    render() {
        return (
            <>
           <Row style={{'height': '23rem' }}>
                    <h3 style={{ color: "white" }}>Enter the amount each participant has to pay</h3>
                    <Row.FixedHeight style={{'marginTop':'4rem'}}>
                        <Col>
                            <InputNumber
                                formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                onChange={this.props.onDaiAmountChange}
                                value={this.props.amountPerPersonInDai}
                            />
                        </Col>
                    </Row.FixedHeight>
                </Row>
                <Row.FixedHeight>
                    <Col>
                        <Button type="primary" onClick={this.props.prevStep} style={{ marginRight: '2rem' }}>  <Icon type="left" /> </Button>
                        <Button className="fourth-create-step" type="primary" onClick={this.props.nextStep}>  <Icon type="right" /> </Button>
                    </Col>
                </Row.FixedHeight>

            </>
        )
    }
}
export default AmountUSD

