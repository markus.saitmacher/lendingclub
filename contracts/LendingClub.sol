pragma solidity 0.5.3;

import "@openzeppelin/contracts-ethereum-package/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts-ethereum-package/contracts/token/ERC20/ERC20.sol";

/**
 * @title LendingClub
 * @dev this contract is the implementation of a LendingCLub on Ethereum
 * It used dai and Compound to earn interest while people put funds into it
 * after a certain period of time all the funds will be sent to a winner
 * who is randomly determined
 */
contract LendingClub  {

  using SafeMath for uint;

  address public owner;

  // TODO add explanations here
  uint public amountPerPerson;
  uint public timeIntervalSeconds;
  uint public totalLendingAmountPerRound;
  uint public currentRound;
  uint public currentContractDaiHoldings;
  uint public nextPayoutPossibleAtSecond;

  //the interface for the ERC20
  ERC20 public underlyingDaiInterface;

  address[] public potentialWinners;
  address[] public contractParticipants;


  event lendingClubInitializedEvent (
    uint indexed _totalLendingAmountPerRoundInDai
  );

  event withdrawBackEvent (
    address indexed _withdrawer,
    uint indexed _amount
  );
  event resetEvent (
    address indexed _sender
  );

  event participantPayedEvent(
    address indexed _sender,
    uint indexed _amount
  );

  event participantWonEvent(
    address indexed _winner,
    uint indexed _winnerId
  );

  /**
    * @dev Initializes the contract setting the deployer as the initial owner.
    */
  constructor (address _daiTokenAddress) public {
      owner = msg.sender;
      underlyingDaiInterface = ERC20(_daiTokenAddress);
      //todo add cdai
  }

  /**
    * @dev Used to to initialize new round and for test purposes
    * @notice Reverts if the sender is not the owner
    */
  function reset() public {
    require(msg.sender == owner, "Tx caller is not the owner of the contract");
    amountPerPerson = 0;
    timeIntervalSeconds = 0;
    totalLendingAmountPerRound = 0;
    currentRound = 0;
    currentContractDaiHoldings = 0;
    nextPayoutPossibleAtSecond = 0;
    address[] memory _emptyArray;
    potentialWinners = _emptyArray;
    contractParticipants = _emptyArray;
    emit resetEvent(msg.sender);
  }

  /**
    * @dev Withdrawing back to owner of the contract
    * @notice Reverts if the sender is not the owner
    */
  function withdrawContractDaiBalanceToOwner() public {
    require(msg.sender == owner, "Tx caller is not the owner of the contract");
    address _contractAddress = address(this);
    uint256 _contractBalance = underlyingDaiInterface.balanceOf(_contractAddress);
    // not trusting currentContractDaiHoldings to be correct
    bool _transferSuccess = underlyingDaiInterface.transfer(msg.sender, _contractBalance);
    require(_transferSuccess == true, "withdrawContractDaiBalanceToOwner transfer was not successfull");
    currentContractDaiHoldings = 0;
    emit withdrawBackEvent(msg.sender, _contractBalance);
  }


  /**
    * @notice Lets make this internal for initial tx, or make this in constructor function
    * @dev This Initializes the contract with set details that can not be changed later
    * @notice Using unsafe block time for now, we store the unix time second future time when the first payout is possible
    * @param _contractParticipants Array of the eth addresses of all Lending club participants
    * @param _amountPerPersonInDai the amount each person lends per round in wei
    * @param _timeIntervalSeconds time after which a payout can occure
    */
  function initializeLendingClub (address[] memory _contractParticipants, uint _amountPerPersonInDai,  uint _timeIntervalSeconds) public {
    require(potentialWinners.length == 0, "Currently within Lending Round");
    require(potentialWinners.length < 127, "We are keeping it small to avoid uint8 bugs");
    contractParticipants = _contractParticipants;
    potentialWinners = _contractParticipants;
    amountPerPerson = _amountPerPersonInDai;
    totalLendingAmountPerRound = _amountPerPersonInDai * _contractParticipants.length;
    nextPayoutPossibleAtSecond = block.timestamp + _timeIntervalSeconds;
    timeIntervalSeconds = _timeIntervalSeconds;

    emit lendingClubInitializedEvent(totalLendingAmountPerRound);
  }

  /**
    * @dev This function can be called by each participant until the full
    * @notice put address in constructor later on
    * @notice Depositing an amount higher than amountPerPerson shouldn't be a problem
    * @notice We are essentailly alway keeping the lending amount inside to earn interest until it is funded next
    * @notice Hence on the last deposit we withdraw to the two last participants
    * @param _daiTokensAmount the value in Dai the caller whishes to add to the lending round and convert to cDai
    */
  function depositDaiToLendingClub(uint _daiTokensAmount) public {
    address _from = msg.sender;
    address _contractAddress = address(this);

    //TODO: keep track of the nonces
    require(potentialWinners.length > 0, "Everyone has already been payed out");
    require(_daiTokensAmount < underlyingDaiInterface.allowance(_from, _contractAddress), "Insufficient allowance for contract");
    require(block.timestamp >= nextPayoutPossibleAtSecond, "The contract can not be paid yet");

    underlyingDaiInterface.transferFrom(_from, _contractAddress, _daiTokensAmount);
    currentContractDaiHoldings += _daiTokensAmount;

    emit participantPayedEvent(_from, _daiTokensAmount);

    // We ony continue with payouts if the fullLendingAmountPerRound has been reached
    // If that is the case we increment the round and reset the currentContractDaiHoldings, and set startTime to now
    if (currentContractDaiHoldings >= totalLendingAmountPerRound) {
      currentRound++;
      currentContractDaiHoldings = 0;
      nextPayoutPossibleAtSecond = block.timestamp + timeIntervalSeconds;
    } else {
      return;
    }
    if (currentRound == 1) {
      // TODO convert to CDAI
    }
    if (currentRound > 1) {
      _withdrawToRandomParticipant();
    }
    // very important that we set this var here to check if we are currently in the last round
    bool _lastRoundReached = currentRound == contractParticipants.length;
    // bool _lastRoundReached = potentialWinners.length == 2;
    if (_lastRoundReached) {
      // we have reached the last round so we payout the remaing amount to the last potentialWinner
      // convert remaing cDai and withdraw to last winner and interest to all participants
      _withdrawToRandomParticipant();
    }
  }


  /**
    * @dev This function is called after depositing after the first lending Round to payout to one random participant
    * @notice could double checking with for loop if everyone payed the correct ammount
    */
  function _withdrawToRandomParticipant() internal {
    require(potentialWinners.length > 0, "No Winners available, contract has ended");
    uint _indexRange = potentialWinners.length - 1;
    uint _winnerIndex = _getRandomIndex(_indexRange);
    address _winnerAddress = potentialWinners[_winnerIndex];
    uint _payoutAmount = totalLendingAmountPerRound;

    bool _transferSuccess = underlyingDaiInterface.transfer(_winnerAddress, _payoutAmount);
    require(_transferSuccess == true, "withdrawToRandomParticipant transfer was not successfull");

    _removeFromPotentialWinners(_winnerIndex);
    emit participantWonEvent(_winnerAddress, _winnerIndex);
  }


  /**
    * @dev Pseudo random function to determin a winner for the current lending round
    * @notice Current block.difficulty and block.timestamp are being _hashed and encoded as a number.
    * @notice After encoding 256 bit hash to 256 bit integer, we are taking reminder by dividing 251,
    * @notice To get an integer in a range from [0, 250]. In our case 251 is prime number
    * @notice Hashing is not important here just using it to get fixed length bits.
    * @notice Biggest Problem is that both block.timestamp and block.difficulty are dependend on miners!
    * @param _indexRange is the range for which we get a random number
    * @return _randomIndex within the range
    */
  function _getRandomIndex(uint _indexRange) private view returns (uint8) {
      uint8 _randomIndex = 0;
      if (potentialWinners.length > 1) {
        bytes memory _packedEncodedEntropy = abi.encodePacked(block.timestamp, block.difficulty);
        bytes32 _hashed = keccak256(_packedEncodedEntropy);
        uint256 _positiveNumberFromBytes = uint256(_hashed);
        uint256 _moduloRandomNumber = _positiveNumberFromBytes % _indexRange;
        _randomIndex = uint8(_moduloRandomNumber); //converstion might be unnecessary
        return _randomIndex;
      } else {
        //only one person left return index 0
        return _randomIndex;
      }
  }

  /**
    * @dev This function removes the last winner of the lending Payout from the potentialWinners array
    * @notice We are overwritting the current winner with the last in the index
    * @notice then we delete the last one as it is a publicate
    * @notice finally, we manually adjusting the array length
    * @param _winnerIndex the array index number of the last winner
    */
  function _removeFromPotentialWinners(uint _winnerIndex) internal {
      require(_winnerIndex < potentialWinners.length, "Invalid winners index");
      potentialWinners[_winnerIndex] = potentialWinners[potentialWinners.length-1];
      delete potentialWinners[potentialWinners.length-1];
      potentialWinners.length--;
    }


  /**
    * @notice Helper function to get the  contractParticipants length
    * @return The Length of the contractParticipants array
    */
  function getContractParticipantsCount() public returns(uint) {
        return contractParticipants.length;
  }

  /**
    * @notice Helper function to get the potentialWinners length
    * @return The Length of the potentialWinners array
    */
  function getPotentialWinnersCount() public returns(uint) {
        return potentialWinners.length;
  }
}

