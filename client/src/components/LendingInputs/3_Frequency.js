import React, { Component } from 'react'
// import { Form, Button } from 'antd'
import { Row, Col, Slider, Button, Icon, Radio } from 'antd';
import styled from 'styled-components'

const Label = styled.label`
  letter-spacing: .02rem;
  margin-bottom: .5rem;
`

const RowFixedHeight = styled(Row)`
    height: 75px !important;
`
const RowCentered = styled(Row)`
    margin-left: 25% !important;
    width: 300px;
`
Row.Centered = RowCentered
Row.FixedHeight = RowFixedHeight

class Frequency extends Component {
    // state = {
    //     show: 'no',
    //     dayIntervalTime: 0,
    //     hoursIntervalTime: 0,
    //     secondsIntervalTime: 0,
    // }

    // onIntervalChange = (value, intervalType) => {
    //     if (!isNaN(value)) {
    //         this.setState({ [intervalType]: value })
    //     }
    // }
    // handleShowChange = e => {
    //     this.setState({ show: e.target.value });
    // };


    render() {
        return (
            <>
                <Row style={{'height': '23rem' }}>
                    {/* <Row.FixedHeight style={{ 'width': '300px', 'margin-top': '50px', 'margin-bottom': '50px' }}> */}
                        <h3 style={{ color: "white" }}>Would you like to set a time limit?</h3>
                    <Row.FixedHeight style={{'marginTop':'1rem'}}>
                        <Radio.Group value={this.props.show} onChange={this.props.handleShowChange}>
                            <Radio.Button value="no">No</Radio.Button>
                            <Radio.Button value="yes">Yes</Radio.Button>
                        </Radio.Group>
                    </Row.FixedHeight>
                    {
                        this.props.show === 'no' ? <></> :
                            <>
                                {/* <Row.FixedHeight style={{ 'width': '300px', 'margin-top': '50px' }}> */}
                                {/* <Row.FixedHeight style={{ 'width': '300px',  }}> */}
                                <Row.Centered>
                                    <Col>
                                        <Label htmlFor={'daysIntervalTime'}>
                                            {'Days'}
                                            <Col span={12}>
                                                <Slider
                                                    name='daysIntervalTime'
                                                    min={0}
                                                    max={31}
                                                    onChange={(value) => this.props.onIntervalChange(value, 'daysIntervalTime')}
                                                    value={this.props.daysIntervalTime}
                                                />
                                            </Col>
                                        </Label>
                                    </Col>
                                </Row.Centered>
                                <Row.Centered>
                                    <Col>
                                        <Label htmlFor={'hoursIntervalTime'}>
                                            {'Hours'}
                                            <Col span={12}>
                                                <Slider
                                                    name='hoursIntervalTime'
                                                    min={0}
                                                    max={24}
                                                    onChange={(value) => this.props.onIntervalChange(value, 'hoursIntervalTime')}
                                                    value={this.props.hoursIntervalTime}
                                                />
                                            </Col>
                                        </Label>
                                    </Col>
                                </Row.Centered>
                                <Row.Centered>
                                    <Col>
                                        <Label htmlFor={'minutesIntervalTime'}>
                                            {'Minutes'}
                                            <Col span={12}>
                                                <Slider
                                                    name='minutesIntervalTime'
                                                    min={0}
                                                    max={60}
                                                    onChange={(value) => this.props.onIntervalChange(value, 'minutesIntervalTime')}
                                                    value={this.props.minutesIntervalTime}
                                                />
                                            </Col>
                                        </Label>
                                    </Col>
                                </Row.Centered>
                                {/* <Row style={{ 'width': '300px' }}> */}
                                <Row style={{ 'marginTop': '30px' }}>
                                    <Col>
                                        <p>{`Interval Time: ${this.props.daysIntervalTime} days, ${this.props.hoursIntervalTime} hours, ${this.props.minutesIntervalTime} minutes`}</p>
                                    </Col>
                                </Row>
                            </>
                    }
                </Row>
                <Row.FixedHeight>
                    <Col>
                        <Button type="primary" onClick={this.props.prevStep} style={{ marginRight: '2rem' }}>  <Icon type="left" /> </Button>
                        <Button className="third-create-step" type="primary" onClick={this.props.nextStep}>  <Icon type="right" /> </Button>
                    </Col>
                </Row.FixedHeight>
            </>
        )
    }
}
export default Frequency
