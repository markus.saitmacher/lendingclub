import { parseError, isTrue  } from "../helpers/utilities"
import { web3SetHttpProvider, web3Instance } from "../helpers/web3"
import { notificationShow } from "./_notification"
import {
  apiGetTransaction,
  apiCreateLendingClub,
  apiGetUserAndHisLendingClub,
  apiGetEthAndDaiTestFunds,
  apiUserPaidLendingClub,
  apiGetLendingClubArtifacts,
  apiGetDaiTokenArtifacts,
} from "../helpers/api"
import { metamaskClearState } from "./_metamask"
// import { getPublicVariableFromContract, resetLendingClubContract } from "../helpers/contractInteractions"
// import LendingClubJSON from "../contracts/LendingClub.json";
// import DaiTokenJSON from "../contracts/DaiToken.json";
const contract = require('truffle-contract')

// -- Constants ------------------------------------------------------------- //
const GOT_CONTRACT_WINNERS_EVENT =
  "account/GOT_CONTRACT_WINNERS_EVENT"
const ACCOUNT_PAY_LENDING_CLUB_REQUEST =
  "account/ACCOUNT_PAY_LENDING_CLUB_REQUEST"
const ACCOUNT_PAY_LENDING_CLUB_SUCCESS =
  "account/ACCOUNT_PAY_LENDING_CLUB_SUCCESS"
const ACCOUNT_PAY_LENDING_CLUB_FAILURE =
  "account/ACCOUNT_PAY_LENDING_CLUB_FAILURE"

const ACCOUNT_CHECK_TRANSACTION_STATUS_REQUEST =
  "account/ACCOUNT_CHECK_TRANSACTION_STATUS_REQUEST"
const ACCOUNT_CHECK_TRANSACTION_STATUS_SUCCESS =
  "account/ACCOUNT_CHECK_TRANSACTION_STATUS_SUCCESS"
const ACCOUNT_CHECK_TRANSACTION_STATUS_FAILURE =
  "account/ACCOUNT_CHECK_TRANSACTION_STATUS_FAILURE"

const ACCOUNT_GET_ETH_AND_DAI_TEST_FUNDS_REQUEST =
  "account/ACCOUNT_GET_ETH_AND_DAI_TEST_FUNDS_REQUEST"
const ACCOUNT_GET_ETH_AND_DAI_TEST_FUNDS_SUCCESS =
  "account/ACCOUNT_GET_ETH_AND_DAI_TEST_FUNDS_SUCCESS"
const ACCOUNT_GET_ETH_AND_DAI_TEST_FUNDS_FAILURE =
  "account/ACCOUNT_GET_ETH_AND_DAI_TEST_FUNDS_FAILURE"


const ACCOUNT_REQUEST_CREATE_NEW_LENDING_CLUB =
  "account/ACCOUNT_REQUEST_CREATE_NEW_LENDING_CLUB"

const ACCOUNT_CREATE_LENDING_CLUB_REQUEST =
  "account/ACCOUNT_CREATE_LENDING_CLUB_REQUEST"
const ACCOUNT_CREATE_LENDING_CLUB_SUCCESS =
  "account/ACCOUNT_CREATE_LENDING_CLUB_SUCCESS"
const ACCOUNT_CREATE_LENDING_CLUB_FAILURE =
  "account/ACCOUNT_CREATE_LENDING_CLUB_FAILURE"

const ACCOUNT_GET_USER_AND_LENDING_CLUB_DATA_REQUEST =
  "account/ACCOUNT_GET_USER_AND_LENDING_CLUB_DATA_REQUEST"
const ACCOUNT_GET_USER_AND_LENDING_CLUB_DATA_SUCCESS =
  "account/ACCOUNT_GET_USER_AND_LENDING_CLUB_DATA_SUCCESS"
const ACCOUNT_GET_USER_AND_LENDING_CLUB_DATA_FAILURE =
  "account/ACCOUNT_GET_USER_AND_LENDING_CLUB_DATA_FAILURE"

const ACCOUNT_UPDATE_ACCOUNT_ADDRESS = "account/ACCOUNT_UPDATE_ACCOUNT_ADDRESS"

const ACCOUNT_UPDATE_NETWORK = "account/ACCOUNT_UPDATE_NETWORK"

const ACCOUNT_UPDATE_PROVIDER = "account/ACCOUNT_UPDATE_PROVIDER"

const ACCOUNT_UPDATE_WEB3 = "account/ACCOUNT_UPDATE_WEB3"

const ACCOUNT_CLEAR_STATE = "account/ACCOUNT_CLEAR_STATE"

// -- Actions --------------------------------------------------------------- //

export const accountCheckTransactionStatus = (txHash, network) => (
  dispatch,
  getState
) => {
  dispatch({ type: ACCOUNT_CHECK_TRANSACTION_STATUS_REQUEST })
  const network = getState().account.network

  apiGetTransaction(txHash, network)
    .then(response => {
      const data = response.data
      if (
        data &&
        !data.error &&
        (data.input === "0x" ||
          (data.input !== "0x" && data.operations && data.operations.length))
      ) {
        dispatch({
          type: ACCOUNT_CHECK_TRANSACTION_STATUS_SUCCESS
        })
      } else {
        setTimeout(
          () => dispatch(accountCheckTransactionStatus(txHash, network)),
          1000
        )
      }
    })
    .catch(error => {
      setTimeout(
        () => dispatch(accountCheckTransactionStatus(txHash, network)),
        1000
      )
      dispatch({ type: ACCOUNT_CHECK_TRANSACTION_STATUS_FAILURE })
      const message = parseError(error)
      dispatch(notificationShow(message, true))
    })
}

export const accountUpdateNetwork = network => dispatch => {
  web3SetHttpProvider(`https://${network}.infura.io/`)
  dispatch({ type: ACCOUNT_UPDATE_NETWORK, payload: network })
}

export const accountUpdateWeb3 = web3 => dispatch => {
  dispatch({ type: ACCOUNT_UPDATE_WEB3, payload: web3 })
}

export const getDaiAndEthTestFunds = () => async (dispatch, getState) => {
  try {
    dispatch({ type: ACCOUNT_GET_ETH_AND_DAI_TEST_FUNDS_REQUEST })
    const { address, network } = getState().account
    const response = await apiGetEthAndDaiTestFunds(address)
    if (response.data.success) {
      dispatch({ type: ACCOUNT_GET_ETH_AND_DAI_TEST_FUNDS_SUCCESS })
    } else {
      throw new Error(`Could not get test funds ${response.data.error}`)
    }
  }
  catch (e) {
    console.error('e', e)
    dispatch({ type: ACCOUNT_GET_ETH_AND_DAI_TEST_FUNDS_FAILURE })
    dispatch(notificationShow(parseError(e), true))
  }
}

export const accountUpdateAccountAddress = (address) => async (
  dispatch,
) => {
  if (!address) return
  dispatch({
    type: ACCOUNT_UPDATE_ACCOUNT_ADDRESS,
    payload: { address }
  })
}

export const accountClearState = () => (dispatch, getState) => {
  const { type } = getState().account
  if (type) {
    switch (type) {
      case "METAMASK":
        dispatch(metamaskClearState())
        break
      default:
        break
    }
  }
  dispatch({ type: ACCOUNT_CLEAR_STATE })
}

export const createLendingClub = (createLendingClubData) => async (dispatch, getState) => {
  try {
    dispatch({ type: ACCOUNT_CREATE_LENDING_CLUB_REQUEST })
    const response = await apiCreateLendingClub(createLendingClubData)
    if (isTrue(response.data.success)) {
      //now we wait until the server has updated and the we fetch the data
      dispatch({
        type: ACCOUNT_CREATE_LENDING_CLUB_SUCCESS,
        payload: response.data.userAndLendingClubData,
      })
    } else {
      throw new Error(`Could not create Lending Club, error: ${response.data.error}`)
    }
  }
  catch (e) {
    console.error('createLendingClub', e)
    dispatch({
      type: ACCOUNT_CREATE_LENDING_CLUB_FAILURE,
    })
    dispatch(notificationShow(parseError(e), true))
  }
}

export const requestCreateNewLendingClub = () => (dispatch) => {
  dispatch({ type: ACCOUNT_REQUEST_CREATE_NEW_LENDING_CLUB })
}


export const getUserAndHisLendingClub = (address) => async (dispatch, getState) => {
  try {
    if (!address) {
     address = getState().account.address
    }
    dispatch({ type: ACCOUNT_GET_USER_AND_LENDING_CLUB_DATA_REQUEST })
    const response = await apiGetUserAndHisLendingClub(address)
    if (isTrue(response.data.success)) {
      dispatch({
        type: ACCOUNT_GET_USER_AND_LENDING_CLUB_DATA_SUCCESS,
        payload: response.data.userAndLendingClubData,
      })
    } else {
      //No account for user yet so dont throw error
      if (response.data.message) {
        dispatch({
          type: ACCOUNT_GET_USER_AND_LENDING_CLUB_DATA_FAILURE,
        })
      } else {
        throw new Error(`Could not get User and Lending Club data, error: ${response.data.error}`)
      }
    }
  }
  catch (e) {
    console.error('getUserAndHisLendingClub', e)
    dispatch({
      type: ACCOUNT_GET_USER_AND_LENDING_CLUB_DATA_FAILURE,
    })
    dispatch(notificationShow(parseError(e), true))
  }
}
const listenForWonEvents = async (lcInstance) => {
  // initialize listen for winning events
  const participantWonEventArray = await lcInstance.getPastEvents('participantWonEvent', {
    fromBlock: 'latest',
    toBlock: 'latest'
  })
  let winners = []
  participantWonEventArray.forEach(event => {
     const winner = event.returnValues._winner
     winners.push(winner)
  })
  // dispatch({type: GOT_CONTRACT_WINNERS_EVENT, payload: winners})
  return winners

}

export const payLendingClub = (amount) => async (dispatch, getState) => {
  try {
    dispatch({ type: ACCOUNT_PAY_LENDING_CLUB_REQUEST })
    const { web3, address, network } = getState().account
    //todo convert amount to 10 ** 18

    const networkId = await web3.eth.net.getId()
    //TODO cache this
    const LendingClubJSON = await apiGetLendingClubArtifacts()
    const DaiTokenJSON = await apiGetDaiTokenArtifacts()

    const lcNetworkData = LendingClubJSON.networks[networkId]
    const daiNetworkData = DaiTokenJSON.networks[networkId]
    if (!lcNetworkData || !daiNetworkData) {
      throw new Error(`Contract not deployed on network with id ${networkId}, please change \
                        your metamask network to point to blockchain.lendingclubdapp.com`)
    }
    const lendingClubAddress = lcNetworkData.address
    const daiTokenAddress = daiNetworkData.address

    const daiInstance = new web3.eth.Contract(DaiTokenJSON.abi, daiTokenAddress)
    const lcInstance = new web3.eth.Contract(LendingClubJSON.abi, lendingClubAddress)

    const allowance = await daiInstance.methods.allowance(address, lendingClubAddress).call()
    if (amount > allowance) {
      const highAmount = amount * 10 //to be safe
      const allowanceTx = await daiInstance.methods.approve(lendingClubAddress, highAmount).send({ from: address })
    }

    // const resp = await lcInstance.methods.depositDaiToLendingClub(amount).send({ from: address })
    const txHash = await new Promise((resolve, reject) => lcInstance.methods
      .depositDaiToLendingClub(amount)
      .send({ from: address })
      .once('transactionHash', resolve)
      .on('error', reject)
    );
    // const winners = await listenForWonEvents(lcInstance)
    // console.log('winners', winners)
    // const receipt = await web3.eth.getTransactionReceipt(txHash)
    // console.log('receipt', receipt)
    await apiUserPaidLendingClub(amount, txHash, lendingClubAddress)
    dispatch(getUserAndHisLendingClub(address))
    // const reset = await lcInstance.methods.reset().send({from: address})
    // console.log('reset', reset)
    // const userDaiBalance = await daiInstance.methods.balanceOf(address).call()
    // const contractDaiBalance = await daiInstance.methods.balanceOf(lendingClubAddress).call()

    dispatch({ type: ACCOUNT_PAY_LENDING_CLUB_SUCCESS })
  } catch (error) {
    console.error('PayLendingClub ', error)
    dispatch({ type: ACCOUNT_PAY_LENDING_CLUB_FAILURE })
    dispatch(notificationShow(parseError(error), true))
  }




}


export // -- Reducer --------------------------------------------------------------- //
  const INITIAL_STATE = {
    network: "kovan",
    provider: null,
    web3: null,
    lendingClubContractData: null,
    fetching: false,
    fetchingFunds: false,
    fetchingCreateLendingClub: false,
    fetchingPayLendingClub: false,
    address: "",
    winners: null,
  }

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ACCOUNT_CREATE_LENDING_CLUB_REQUEST:
      return { ...state, fetchingCreateLendingClub: true }
    case ACCOUNT_CREATE_LENDING_CLUB_SUCCESS:
      return { ...state, fetchingCreateLendingClub: false, lendingClubContractData: action.payload }
    case ACCOUNT_CREATE_LENDING_CLUB_FAILURE:
      return { ...state, fetchingCreateLendingClub: false }

    case ACCOUNT_REQUEST_CREATE_NEW_LENDING_CLUB:
      return { ...state, lendingClubContractData: null }

    case ACCOUNT_GET_ETH_AND_DAI_TEST_FUNDS_REQUEST:
      return { ...state, fetchingFunds: true }
    case ACCOUNT_GET_ETH_AND_DAI_TEST_FUNDS_SUCCESS:
      return { ...state, fetchingFunds: false }
    case ACCOUNT_GET_ETH_AND_DAI_TEST_FUNDS_FAILURE:
      return { ...state, fetchingFunds: false }

    case ACCOUNT_GET_USER_AND_LENDING_CLUB_DATA_REQUEST:
      return { ...state, fetching: true }
    case ACCOUNT_GET_USER_AND_LENDING_CLUB_DATA_SUCCESS:
      return { ...state, fetching: false, lendingClubContractData: action.payload }
    case ACCOUNT_GET_USER_AND_LENDING_CLUB_DATA_FAILURE:
      return { ...state, fetching: false, lendingClubContractData: null }

    case ACCOUNT_PAY_LENDING_CLUB_REQUEST:
      return { ...state, fetchingPayLendingClub: true }
    case ACCOUNT_PAY_LENDING_CLUB_SUCCESS:
      return { ...state, fetchingPayLendingClub: false }
    case ACCOUNT_PAY_LENDING_CLUB_FAILURE:
      return { ...state, fetchingPayLendingClub: false }

    case ACCOUNT_UPDATE_ACCOUNT_ADDRESS:
      return {
        ...state,
        address: action.payload.address,
      }

    case ACCOUNT_CHECK_TRANSACTION_STATUS_SUCCESS:
      return { ...state, transactions: action.payload }
    case ACCOUNT_UPDATE_NETWORK:
      return { ...state, network: action.payload }
    case ACCOUNT_UPDATE_PROVIDER:
      return { ...state, provider: action.payload }
    case ACCOUNT_UPDATE_WEB3:
      return { ...state, web3: action.payload }
    case ACCOUNT_CLEAR_STATE:
      return { ...state, ...INITIAL_STATE }
    default:
      return state
  }
}
