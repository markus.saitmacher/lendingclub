
export const getPublicVariableFromContract = async (contract, variable) => {
    const contractVariable = await contract[variable].call()
    return contractVariable
}
export const resetLendingClubContract = async (lc, account) => {
    const resetTx = await lc.reset({ from: account })
    return resetTx
}
